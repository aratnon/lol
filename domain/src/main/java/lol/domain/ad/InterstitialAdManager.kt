package lol.domain.ad

import io.reactivex.Single
import lol.domain.repository.PrefsHelper
import javax.inject.Inject

class InterstitialAdManager constructor(private val prefsHelper: PrefsHelper) {

    private val showTarget = 3
    private var adCount = 0
    private lateinit var interstitialAd: BaseInterstitial

    fun setInterstitialAd(interstitial: BaseInterstitial)
    {
        this.interstitialAd = interstitial
    }

    fun canShow(): Single<Boolean> =
            Single.just(interstitialAd.isLoaded() && adCount % showTarget == 0)

    fun increaseAdCount() {
        ++adCount
    }


    fun show(listener: BaseInterstitial.InterstitialAdListener)
    {
        interstitialAd.show(listener)
    }

    fun load()
    {
        interstitialAd.load(prefsHelper.hasAgreeConsent())
    }

    fun isLoaded(): Single<Boolean> = Single.just(interstitialAd.isLoaded())

}