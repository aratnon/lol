package lol.domain.ad

import io.reactivex.Single

interface BaseInterstitial {
    interface InterstitialAdListener
    {
        fun onAdClosed()
    }

    fun show(listener: InterstitialAdListener)
    fun load(personalized: Boolean)
    fun isLoaded(): Boolean
}