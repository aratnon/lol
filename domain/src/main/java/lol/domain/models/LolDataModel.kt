package lol.domain.models

data class LolDataModel(var championString: String = "",
                        var itemString: String = "",
                        var runeString: String = "")
