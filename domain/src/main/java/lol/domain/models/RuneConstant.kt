package lol.domain.models

class RuneConstant {
    companion object {
        const val CATEGORY_PRECISION = "Precision"
        const val CATEGORY_DOMINATION = "Domination"
        const val CATEGORY_SORCERY = "Sorcery"
        const val CATEGORY_INSPIRATION = "Inspiration"
        const val CATEGORY_RESOLVE = "Resolve"
    }
}