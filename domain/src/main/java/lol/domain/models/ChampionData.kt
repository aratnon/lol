package lol.domain.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ChampionData(
        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("key")
        val key: String,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("title")
        val title: String,
        @Expose
        @SerializedName("image")
        val image: Image,
        @Expose
        @SerializedName("skins")
        val skins: List<Skins>,
        @Expose
        @SerializedName("lore")
        val lore: String,
        @Expose
        @SerializedName("blurb")
        val blurb: String,
        @Expose
        @SerializedName("allytips")
        val allytips: List<String>,
        @Expose
        @SerializedName("enemytips")
        val enemytips: List<String>,
        @Expose
        @SerializedName("tags")
        val tags: List<String>,
        @Expose
        @SerializedName("partype")
        val partype: String,
        @Expose
        @SerializedName("info")
        val info: Info,
        @Expose
        @SerializedName("stats")
        val stats: Stats,
        @Expose
        @SerializedName("spells")
        val spells: List<Spells>,
        @Expose
        @SerializedName("passive")
        val passive: Passive,
        @Expose
        @SerializedName("recommended")
        val recommended: List<Recommended>)

data class Recommended(
        @Expose
        @SerializedName("champion")
        val champion: String,
        @Expose
        @SerializedName("title")
        val title: String,
        @Expose
        @SerializedName("type")
        val type: String,
        @Expose
        @SerializedName("map")
        val map: String,
        @Expose
        @SerializedName("mode")
        val mode: String,
        @Expose
        @SerializedName("blocks")
        val blocks: List<Blocks>)

data class Blocks(
        @Expose
        @SerializedName("type")
        val type: String,
        @Expose
        @SerializedName("recMath")
        val recMath: Boolean,
        @Expose
        @SerializedName("items")
        val items: List<Items>)

data class Items(
        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("count")
        val count: Int)

data class Passive(
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("sanitizedDescription")
        val sanitizedDescription: String,
        @Expose
        @SerializedName("image")
        val image: Image)

data class Spells(
        @Expose
        @SerializedName("name")
        val name: String?,
        @Expose
        @SerializedName("description")
        val description: String,
        @Expose
        @SerializedName("sanitizedDescription")
        val sanitizedDescription: String,
        @Expose
        @SerializedName("tooltip")
        val tooltip: String,
        @Expose
        @SerializedName("sanitizedTooltip")
        val sanitizedTooltip: String,
        @Expose
        @SerializedName("leveltip")
        val leveltip: Leveltip,
        @Expose
        @SerializedName("image")
        val image: Image,
        @Expose
        @SerializedName("resource")
        val resource: String,
        @Expose
        @SerializedName("maxrank")
        val maxrank: Int,
        @Expose
        @SerializedName("cost")
        val cost: List<Int>,
        @Expose
        @SerializedName("costType")
        val costType: String,
        @Expose
        @SerializedName("costBurn")
        val costBurn: String,
        @Expose
        @SerializedName("cooldown")
        val cooldown: List<Double>,
        @Expose
        @SerializedName("cooldownBurn")
        val cooldownBurn: String,
        @Expose
        @SerializedName("effect")
        val effect: List<List<Double>>,
        @Expose
        @SerializedName("effectBurn")
        val effectBurn: List<String>,
        @Expose
        @SerializedName("vars")
        val vars: List<Vars>,
        @Expose
        @SerializedName("range")
        val range: List<Int>,
        @Expose
        @SerializedName("rangeBurn")
        val rangeBurn: String,
        @Expose
        @SerializedName("key")
        val key: String)

data class Vars(
        @Expose
        @SerializedName("key")
        val key: String,
        @Expose
        @SerializedName("link")
        val link: String,
        @Expose
        @SerializedName("coeff")
        val coeff: List<Double>)

data class Leveltip(
        @Expose
        @SerializedName("label")
        val label: List<String>,
        @Expose
        @SerializedName("effect")
        val effect: List<String>)

data class Stats(
        @Expose
        @SerializedName("armor")
        val armor: Double,
        @Expose
        @SerializedName("armorperlevel")
        val armorperlevel: Double,
        @Expose
        @SerializedName("attackdamage")
        val attackdamage: Double,
        @Expose
        @SerializedName("attackdamageperlevel")
        val attackdamageperlevel: Double,
        @Expose
        @SerializedName("attackrange")
        val attackrange: Double,
        @Expose
        @SerializedName("attackspeedoffset")
        val attackspeedoffset: Double,
        @Expose
        @SerializedName("attackspeedperlevel")
        val attackspeedperlevel: Double,
        @Expose
        @SerializedName("crit")
        val crit: Double,
        @Expose
        @SerializedName("critperlevel")
        val critperlevel: Double,
        @Expose
        @SerializedName("hp")
        val hp: Double,
        @Expose
        @SerializedName("hpperlevel")
        val hpperlevel: Double,
        @Expose
        @SerializedName("hpregen")
        val hpregen: Double,
        @Expose
        @SerializedName("hpregenperlevel")
        val hpregenperlevel: Double,
        @Expose
        @SerializedName("movespeed")
        val movespeed: Double,
        @Expose
        @SerializedName("mp")
        val mp: Double,
        @Expose
        @SerializedName("mpperlevel")
        val mpperlevel: Double,
        @Expose
        @SerializedName("mpregen")
        val mpregen: Double,
        @Expose
        @SerializedName("mpregenperlevel")
        val mpregenperlevel: Double,
        @Expose
        @SerializedName("spellblock")
        val spellblock: Double,
        @Expose
        @SerializedName("spellblockperlevel")
        val spellblockperlevel: Double)

data class Info(
        @Expose
        @SerializedName("attack")
        val attack: Int,
        @Expose
        @SerializedName("defense")
        val defense: Int,
        @Expose
        @SerializedName("magic")
        val magic: Int,
        @Expose
        @SerializedName("difficulty")
        val difficulty: Int)

data class Skins(
        @Expose
        @SerializedName("id")
        val id: Int,
        @Expose
        @SerializedName("name")
        val name: String,
        @Expose
        @SerializedName("num")
        val num: Int)

data class Image(
        @Expose
        @SerializedName("full")
        val full: String,
        @Expose
        @SerializedName("sprite")
        val sprite: String,
        @Expose
        @SerializedName("group")
        val group: String,
        @Expose
        @SerializedName("x")
        val x: Int,
        @Expose
        @SerializedName("y")
        val y: Int,
        @Expose
        @SerializedName("w")
        val w: Int,
        @Expose
        @SerializedName("h")
        val h: Int)

