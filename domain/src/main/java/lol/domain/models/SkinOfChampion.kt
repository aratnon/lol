package lol.domain.models

data class SkinOfChampion(val skinName: String,
                          val skinFileName: String,
                          val skinUrl: String)