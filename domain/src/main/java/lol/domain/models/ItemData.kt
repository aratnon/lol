package lol.domain.models

import com.google.gson.annotations.SerializedName

data class ItemData(
    @SerializedName("stats") val stats: Stats,
    @SerializedName("name") val name: String,
    @SerializedName("gold") val gold: Gold?,
    @SerializedName("tags") val tags: List<String>,
    @SerializedName("plaintext") val plaintext: String,
    @SerializedName("image") val image: Image,
    @SerializedName("sanitizedDescription") val sanitizedDescription: String,
    @SerializedName("id") val id: Int,
    @SerializedName("into") val into: List<String>,
    @SerializedName("from") val from: List<String>,
    @SerializedName("description") val description: String,
    var imageUrl: String
) {

    fun getGoldString(): String
        = "${gold!!.total.toString()}(${gold.sell.toString()})"
//    data class Stats(
//        @SerializedName("FlatMovementSpeedMod") val flatMovementSpeedMod: Int?
//    )

    data class Gold(
        @SerializedName("total") val total: Int,
        @SerializedName("sell") val sell: Int,
        @SerializedName("base") val base: Int,
        @SerializedName("purchasable") val purchasable: Boolean
    )

    data class Image(
        @SerializedName("full") val full: String,
        @SerializedName("group") val group: String,
        @SerializedName("sprite") val sprite: String,
        @SerializedName("h") val h: Int,
        @SerializedName("w") val w: Int,
        @SerializedName("y") val y: Int,
        @SerializedName("x") val x: Int
    )
}