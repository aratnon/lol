package lol.domain.models

import com.google.gson.annotations.SerializedName

data class RuneData(
        @SerializedName("name") val name: String,
        @SerializedName("id") val id: Int,
        @SerializedName("key") val key: String,
        @SerializedName("shortDesc") val shortDesc: String,
        @SerializedName("longDesc") val longDesc: String,
        @SerializedName("icon") val icon: String,
        var runeImageUrl: String
)