package lol.domain.repository

import io.reactivex.Completable
import io.reactivex.Single

interface PrefsHelper {

    fun setLoLVersion(version: String)
    fun getLoLVersion(): Single<String>
    fun hasInitData(): Boolean
    fun setHasInitData(hasInitData: Boolean)

    fun hasConsent(): Boolean
    fun hasAgreeConsent(): Boolean
    fun setConsent(agree: Boolean): Completable
}