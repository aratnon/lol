package lol.domain.repository

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import lol.domain.models.LolDataModel
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*


interface LoLRemoteService {

    fun downloadLoLVersion(): Single<String>
    fun downloadLolData(version: String): Single<LolDataModel>

}