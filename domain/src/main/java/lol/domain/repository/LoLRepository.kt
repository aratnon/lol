package lol.domain.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import lol.domain.models.ChampionData
import lol.domain.models.ItemData
import lol.domain.models.RuneData
import org.reactivestreams.Subscription

interface LoLRepository {

    fun initData(): Completable
    fun getChampionData(championId: String): Flowable<ChampionData>
    fun getAllChampionData(): Flowable<MutableList<ChampionData>>
    fun getAllItemData(): Flowable<MutableList<ItemData>>
    fun getItemData(itemId: String): Flowable<ItemData>
    fun getMultipleItemData(itemIds: List<String>): Flowable<MutableList<ItemData>>
    fun getRune(runeId: Int): Flowable<RuneData>
    fun getAllRuneDataByCategory(category: String): Flowable<MutableList<MutableList<RuneData>>>
    fun getRuneCategoryImageId(category: String): Flowable<Int>
    fun downloadFile(url: String, fileName: String): Completable
}