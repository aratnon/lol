package lol.domain.repository

import io.reactivex.Completable
import io.reactivex.Single

interface RuneImageRepository {
    fun init(): Completable
    fun getRuneImage(runeId: Int): Int?
}