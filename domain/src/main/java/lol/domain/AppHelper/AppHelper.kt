package lol.domain.AppHelper

import io.reactivex.Completable
import io.reactivex.Single

interface AppHelper {
    fun hasInternetConnection(): Boolean
    fun getAppVersion(): Single<String>
    fun showDunidle(): Completable
    fun showReview(): Completable
    fun showContactMe(): Completable
}