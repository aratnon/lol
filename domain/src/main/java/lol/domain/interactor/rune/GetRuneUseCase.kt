package lol.domain.interactor.rune

import io.reactivex.Flowable
import lol.domain.models.RuneData
import lol.domain.repository.LoLRepository
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class GetRuneUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                         private val loLRepository: LoLRepository) {

    fun execute(runeId: Int): Flowable<RuneData>
        = loLRepository.getRune(runeId)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.android())

}