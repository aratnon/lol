package lol.domain.interactor.rune

import io.reactivex.Flowable
import lol.domain.models.RuneData
import lol.domain.repository.LoLRepository
import lol.domain.repository.RuneImageRepository
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class GetRuneCategoryUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                                 private val loLRepository: LoLRepository){

    fun execute(category: String): Flowable<Int>
    {
        return loLRepository.getRuneCategoryImageId(category)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.android())
    }
}