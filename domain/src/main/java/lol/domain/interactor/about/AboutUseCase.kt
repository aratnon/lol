package lol.domain.interactor.about

import io.reactivex.Completable
import io.reactivex.Single
import lol.domain.AppHelper.AppHelper
import lol.domain.repository.LoLRepository
import lol.domain.repository.PrefsHelper
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class AboutUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                       private val prefsHelper: PrefsHelper,
                                       private val lolRepository: LoLRepository,
                                       private val appHelper: AppHelper) {


    fun getAppVersion(): Single<String>
        = appHelper.getAppVersion()
            .observeOn(rxSchedulers.io())
            .subscribeOn(rxSchedulers.android())

    fun getLoLVersion(): Single<String>
        = prefsHelper.getLoLVersion()
            .observeOn(rxSchedulers.io())
            .subscribeOn(rxSchedulers.android())

    fun showDunidle(): Completable
        = appHelper.showDunidle()

    fun showReview(): Completable
        = appHelper.showReview()

    fun showContactMe(): Completable
        = appHelper.showContactMe()
}