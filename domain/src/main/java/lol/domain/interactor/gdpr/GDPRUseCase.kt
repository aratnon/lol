package lol.domain.interactor.gdpr

import io.reactivex.Completable
import io.reactivex.Single
import lol.domain.repository.PrefsHelper
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class GDPRUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                      private val prefsHelper: PrefsHelper) {



    fun hasConsent(): Boolean
        = prefsHelper.hasConsent()

    fun saveConsent(agree: Boolean): Completable
        = prefsHelper.setConsent(agree)
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.android())
}