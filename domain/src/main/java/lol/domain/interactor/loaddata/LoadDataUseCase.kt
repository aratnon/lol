package lol.domain.interactor.loaddata

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import lol.domain.AppHelper.AppHelper
import lol.domain.models.ChampionData
import lol.domain.repository.LoLRepository
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class LoadDataUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                          private val lolRepository: LoLRepository,
                                          private val appHelper: AppHelper) {



    fun loadData(): Completable {
        return if(appHelper.hasInternetConnection())
            lolRepository.initData()
                    .subscribeOn(rxSchedulers.io())
                    .observeOn(rxSchedulers.android())
        else
            Completable.error(Throwable("No internet connection"))
                    .observeOn(rxSchedulers.android())
    }

    fun getChampion(): Flowable<ChampionData> = lolRepository.getChampionData("1")
            .subscribeOn(rxSchedulers.io())
            .observeOn(rxSchedulers.android())
}