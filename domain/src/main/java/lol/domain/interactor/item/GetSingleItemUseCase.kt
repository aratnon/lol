package lol.domain.interactor.item

import io.reactivex.Flowable
import lol.domain.models.ItemData
import lol.domain.repository.LoLRepository
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class GetSingleItemUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                               private val loLRepository: LoLRepository)
{
    fun execute(itemId: String): Flowable<ItemData> {
        return loLRepository.getItemData(itemId)
                .map { itemData ->
                    itemData.imageUrl = getItemUrl(itemData.image.full)
                    itemData
                }
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.android())
    }

    private fun setItemUrl(itemData: MutableList<ItemData>): MutableList<ItemData>
    {
        for (item in itemData)
            item.imageUrl = getItemUrl(item.image.full)

        return itemData
    }

    private fun getItemUrl(imageFile: String): String
        = "http://ddragon.leagueoflegends.com/cdn/8.11.1/img/item/$imageFile"
}