package lol.domain.interactor.item

import io.reactivex.Flowable
import lol.domain.models.ItemData
import lol.domain.repository.LoLRepository
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class GetItemListUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                             private val loLRepository: LoLRepository)
{
    fun execute(): Flowable<MutableList<ItemData>> {
        return loLRepository.getAllItemData()
                .map { itemData ->
                    val filterList = itemData.filter { item ->
                        item.gold!!.purchasable
                    }.toMutableList()

                    setItemUrl(filterList)
                }
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.android())
    }

    private fun setItemUrl(itemData: MutableList<ItemData>): MutableList<ItemData>
    {
        for (item in itemData)
            item.imageUrl = getItemUrl(item.image.full)

        return itemData
    }

    private fun getItemUrl(imageFile: String): String
        = "http://ddragon.leagueoflegends.com/cdn/8.11.1/img/item/$imageFile"
}