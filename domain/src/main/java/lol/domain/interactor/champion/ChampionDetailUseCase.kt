package lol.domain.interactor.champion

import io.reactivex.Flowable
import lol.domain.models.*
import lol.domain.repository.LoLRepository
import lol.domain.repository.PrefsHelper
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class ChampionDetailUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                                private val prefsHelper: PrefsHelper,
                                                private val lolRepository: LoLRepository) {


    fun getChampion(championId: String): Flowable<ChampionData> {
        return lolRepository.getChampionData(championId)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.android())
    }

    fun getChampionInfoValue(value: Int): Float = value * 10.0f

    fun getStatString(value: Double, valuePerLevel: Double): String
            = String.format("%s(%s)", value , valuePerLevel)

    fun getChampionName(name: String, title: String): String
            = String.format("%s\n%s", name , title.replace("the", "The"))

    fun getChampionLoadingBackgroundUrl(championKey: String): String
            = "http://ddragon.leagueoflegends.com/cdn/img/champion/loading/${championKey}_0.jpg"

    fun getChampionProfileUrl(championKey: String): String
            = "http://ddragon.leagueoflegends.com/cdn/${prefsHelper.getLoLVersion().blockingGet()}/img/champion/$championKey.png"

    fun getPassiveImageUrl(abilityName: String): String
            = "http://ddragon.leagueoflegends.com/cdn/${prefsHelper.getLoLVersion().blockingGet()}/img/passive/$abilityName"

    fun getAbilityImageUrl(abilityName: String): String
            = "http://ddragon.leagueoflegends.com/cdn/${prefsHelper.getLoLVersion().blockingGet()}/img/spell/$abilityName"

    fun getSkinImageUrl(skin: Skins): String
            = "http://ddragon.leagueoflegends.com/cdn/img/champion/splash/${skin.name}_%${skin.num}.jpg"

    fun getSkinImageUrls(championName:String, skins: List<Skins>): MutableList<String>
    {
        val urls = mutableListOf<String>()
        for(skin in skins)
            urls.add("http://ddragon.leagueoflegends.com/cdn/img/champion/splash/${championName}_${skin.num}.jpg")

        return urls
    }

    private fun getSkinsOfChampion(championData: ChampionData): MutableList<SkinOfChampion>
    {
        val skinsOfChampion = mutableListOf<SkinOfChampion>()
        for(i in 0 until championData.skins.count())
        {
            val skin = championData.skins[i]
            val key = championData.key.replace("'", "")
            val fileName = "${key}_${skin.num}.jpg"
            val url = "http://ddragon.leagueoflegends.com/cdn/img/champion/splash/$fileName"

            when(i)
            {
                0 -> skinsOfChampion.add(SkinOfChampion(championData.name, fileName, url))
                else -> skinsOfChampion.add(SkinOfChampion(skin.name, fileName, url))
            }
        }

        return skinsOfChampion
    }


    fun getChampionSkins(championId: String): Flowable<MutableList<SkinOfChampion>>
    {
        return getChampion(championId).map { championData ->
            getSkinsOfChampion(championData)
        }
    }

    private fun getAbilityToolTip(ability: Spells): String
    {
        if(ability.tooltip == null)
            return ""

        val vars = ability.vars
//        Log.d(ability.name, ability.vars.size.toString())
        var tooltip = ability.tooltip.replace("color", "#")
                .replace("class", "color")
                .replace("span", "font")
                .replace("<scaleAP>", "<font color=\"#99FF99\">")
                .replace("</scaleAP>", "</font>")
                .replace("{{ e1 }}", ability.effectBurn[1])
                .replace("{{ e2 }}", ability.effectBurn[2])
                .replace("{{ e3 }}", ability.effectBurn[3])
                .replace("{{ e4 }}", ability.effectBurn[4])
                .replace("{{ e5 }}", ability.effectBurn[5])
                .replace("{{ e6 }}", ability.effectBurn[6])
                .replace("{{ e7 }}", ability.effectBurn[7])
                .replace("{{ e8 }}", ability.effectBurn[8])
                .replace("{{ e9 }}", ability.effectBurn[9])
                .replace("{{ e10 }}", ability.effectBurn[10])

        for(i in 1 until 20)
        {
            val key = "a$i"
            val replacedString = "{{ $key }}"
//            Log.d("replacedString", replacedString)
            if(tooltip.contains(replacedString))
            {
                for(j in 0 until vars.size)
                {
                    if(vars[j].key == key)
                        tooltip = tooltip.replace(replacedString, vars[j].coeff[0].toString())
                }
            }
        }

        for(i in 1 until 20)
        {
            val key = "f$i"
            val replacedString = "{{ $key }}"
            if(tooltip.contains(replacedString))
            {
                var found = false
                if(vars != null)
                {
                    for(j in 0 until vars.size)
                    {
                        if(vars[j].key == key)
                        {
                            found = true
                            tooltip = tooltip.replace(replacedString, vars[j].coeff[0].toString())
                        }
                    }
                }

                if(!found) tooltip = tooltip.replace(replacedString, "0")
            }
        }


//        if(vars.size >= 1)
//        {
//            tooltip.replace("{{ a1 }}", vars.getOrNull(0)?.coeff?.get(0).toString())
//        }
//
//        if(vars[1] != null)
//        {
//            tooltip.replace("{{ a2 }}", vars.getOrNull(1)?.coeff?.get(0).toString())
//        }

        return tooltip
    }

    fun getAbilityDescription(ability: Spells): String
    {
        return "${ ability.description }<br><br>${ getAbilityToolTip(ability) }"
    }

    public fun getAbilityCooldownText(ability: Spells): String
    {
        return ability.cooldownBurn
    }

    public fun getAbilityCostText(ability: Spells): String
    {
        return "${ ability.costBurn }${ ability.costType }"
    }

    fun getAbilityRangeText(ability: Spells): String
    {
        return ability.rangeBurn
    }

    fun getChampionTags(tags: List<String>): String
    {
        val tagBuilder = StringBuilder()

        for (i in 0 until tags.count()) {
            tagBuilder.append(tags[i])
            if(i != tags.count() - 1)
                tagBuilder.append(", ")
        }

        return tagBuilder.toString()
    }
}