package lol.domain.interactor.champion

import io.reactivex.Flowable
import lol.domain.models.ChampionData
import lol.domain.repository.LoLRepository
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class ChampionListUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                              private val lolRepository: LoLRepository) {


    fun getAllChampions(): Flowable<MutableList<ChampionData>>{
        return lolRepository.getAllChampionData()
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.android())
    }

    fun getProfileUrl(): String{
        return "http://ddragon.leagueoflegends.com/cdn/8.10.1/img/champion/"
    }

}