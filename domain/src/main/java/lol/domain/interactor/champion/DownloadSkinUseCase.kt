package lol.domain.interactor.champion

import io.reactivex.Completable
import lol.domain.repository.LoLRepository
import lol.domain.rx.RxSchedulers
import javax.inject.Inject

class DownloadSkinUseCase @Inject constructor(private val rxSchedulers: RxSchedulers,
                                              private val lolRepository: LoLRepository) {

    fun execute(url: String, fileName: String): Completable
    {
        return lolRepository.downloadFile(url, fileName)
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.android())
    }
}