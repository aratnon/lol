package lol.domain.rx

import io.reactivex.Scheduler

interface RxSchedulers {
    fun io(): Scheduler
    fun android(): Scheduler
}