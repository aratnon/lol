package com.aratnon.lol

import android.app.Application
import com.aratnon.lol.di.*
import com.google.android.gms.ads.MobileAds
import com.google.firebase.FirebaseApp
import timber.log.Timber

class App: Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(this)
        MobileAds.initialize(this, getString(R.string.admob_app_id))

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .roomModule(RoomModule())
                .remoteModule(RemoteModule())
                .build()

        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }
}