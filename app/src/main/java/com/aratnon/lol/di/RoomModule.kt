package com.aratnon.lol.di

import android.content.Context
import com.aratnon.lol.di.scopes.PerApplication
import dagger.Module
import dagger.Provides
import lol.data.database.LoLDatabase
import javax.inject.Singleton

@Module
class RoomModule{
    @Provides @PerApplication fun provideLoLDatabase(context: Context) =
            LoLDatabase.buildDatabase(context)
}