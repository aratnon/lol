package com.aratnon.lol.di

import android.content.Context
import com.aratnon.lol.di.scopes.PerApplication
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import lol.data.repository.LolRemoteServiceImpl
import lol.domain.repository.LoLRemoteService
import lol.domain.rx.RxSchedulers

@Module
class RemoteModule {

    @Provides @PerApplication
    fun provideFirebaseStorage(): FirebaseStorage
            = FirebaseStorage.getInstance()

    @Provides @PerApplication
    fun provideLolRemoteService(context: Context, firebaseStorage: FirebaseStorage, rxSchedulers: RxSchedulers): LoLRemoteService
        = LolRemoteServiceImpl(context, firebaseStorage, rxSchedulers)

}