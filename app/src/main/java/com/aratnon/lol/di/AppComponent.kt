package com.aratnon.lol.di

import com.aratnon.lol.di.scopes.PerApplication
import com.aratnon.lol.ui.dunidle.DunidleFragment
import dagger.Component
import lol.domain.ad.BaseInterstitial
import lol.presentation.about.AboutPresenter
import lol.presentation.champion.abilties.ChampionAbilityPresenter
import lol.presentation.champion.detail.ChampionDetailPresenter
import lol.presentation.champion.list.ChampionListPresenter
import lol.presentation.champion.overview.ChampionOverviewPresenter
import lol.presentation.champion.skin.ChampionSkinPresenter
import lol.presentation.home.HomePresenter
import lol.presentation.item.list.ItemListPresenter
import lol.presentation.item.overview.ItemOverviewPresenter
import lol.presentation.loaddata.LoadDataPresenter
import lol.presentation.rune.list.RuneListPresenter
import lol.presentation.rune.overview.RuneOverviewPresenter

@PerApplication
@Component(modules = [AppModule::class, RoomModule::class, RemoteModule::class])
interface AppComponent {

    fun inject(dunidleFragment: DunidleFragment)

    fun getLoadDataPresenter(): LoadDataPresenter

    fun getHomePresenter(): HomePresenter
    fun getChampionListPresenter(): ChampionListPresenter
    fun getChampionDetailPresenter(): ChampionDetailPresenter
    fun getChampionOverviewPresenter(): ChampionOverviewPresenter
    fun getChampionAbilityPresenter(): ChampionAbilityPresenter
    fun getChampionSkinPresenter(): ChampionSkinPresenter

    fun getItemListPresenter(): ItemListPresenter
    fun getItemOverviewPresenter(): ItemOverviewPresenter

    fun getRuneListPresenter(): RuneListPresenter
    fun getRuneOverviewPresenter(): RuneOverviewPresenter

    fun getAboutPresenter(): AboutPresenter
}