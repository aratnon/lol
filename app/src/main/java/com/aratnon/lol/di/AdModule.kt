package com.aratnon.lol.di

import android.support.v7.app.AppCompatActivity
import com.aratnon.lol.R
import com.aratnon.lol.di.scopes.PerActivity
import lol.data.ad.AdmobInterstitial
import lol.domain.ad.BaseInterstitial
import com.google.android.gms.ads.InterstitialAd
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class AdModule constructor(private val activity: AppCompatActivity) {

    companion object {
        private const val INTERSTITIAL_AD_ID_NAMED: String = "INTERSTITIAL_AD_ID"
        private const val IRON_SOURCE_AD_ID_NAMED: String = "IRON_SOURCE_AD_ID"
    }

    @Provides @PerActivity @Named(INTERSTITIAL_AD_ID_NAMED)
    fun provideInterstitialAdId(): String
        = activity.getString(R.string.interstitial_ad_id)

    @Provides @PerActivity @Named(IRON_SOURCE_AD_ID_NAMED)
    fun provideIronSourceAdId(): String
            = activity.getString(R.string.iron_source_app_key)

    @Provides @PerActivity
    fun provideInterstitialAd(@Named(INTERSTITIAL_AD_ID_NAMED) adId: String): InterstitialAd
    {
        val interstitialAd = InterstitialAd(activity)
        interstitialAd.adUnitId = adId

        return interstitialAd
    }

    @Provides @PerActivity
    fun provideAdmobInterstitialAd(interstitialAd: InterstitialAd): BaseInterstitial
            = AdmobInterstitial(interstitialAd)

//    @Provides @PerActivity
//    fun provideAdmobInterstitialAd(@Named(IRON_SOURCE_AD_ID_NAMED) ironSourceAppKey: String): BaseInterstitial
//    {
//        IronSource.init(activity, ironSourceAppKey, IronSource.AD_UNIT.INTERSTITIAL)
//        Timber.d("init IronSource")
//        return IronSourceAd()
//    }

}