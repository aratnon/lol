package com.aratnon.lol.di

import android.content.Context
import com.aratnon.lol.AppHelperImpl
import com.aratnon.lol.di.scopes.PerApplication
import com.aratnon.lol.repository.RuneImageRepositoryImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import lol.data.database.LoLDatabase
import lol.data.database.PrefsHelperImpl
import lol.data.repository.LoLRepositoryImpl
import lol.domain.AppHelper.AppHelper
import lol.domain.ad.BaseInterstitial
import lol.domain.ad.InterstitialAdManager
import lol.domain.repository.LoLRemoteService
import lol.domain.repository.LoLRepository
import lol.domain.repository.PrefsHelper
import lol.domain.repository.RuneImageRepository
import lol.domain.rx.RxSchedulers
import lol.domain.rx.RxSchedulersImpl

@Module
class AppModule(private val context: Context) {

    @Provides @PerApplication
    fun provideContext(): Context = context

    @Provides @PerApplication
    fun provideAppHelper(context: Context): AppHelper
        = AppHelperImpl(context)

    @Provides @PerApplication
    fun providePrefsHelper(context: Context): PrefsHelper
        = PrefsHelperImpl(context)

    @Provides @PerApplication
    fun provideGson(): Gson = Gson()

    @Provides @PerApplication
    fun provideRxSchedulers(): RxSchedulers = RxSchedulersImpl()

    @Provides @PerApplication
    fun provideRuneImageRepository(): RuneImageRepository
        = RuneImageRepositoryImpl()

    @Provides @PerApplication
    fun provideLoLRepository(context: Context,
                             gson: Gson,
                             rxSchedulers: RxSchedulers,
                             prefsHelper: PrefsHelper,
                             lolDatabase: LoLDatabase,
                             loLRemoteService: LoLRemoteService,
                             runeImageRepository: RuneImageRepository): LoLRepository
        = LoLRepositoryImpl(context,
            gson,
            rxSchedulers,
            prefsHelper,
            lolDatabase,
            loLRemoteService,
            runeImageRepository)

    @Provides @PerApplication
    fun provideInterstitialAdManager(prefsHelper: PrefsHelper): InterstitialAdManager
        = InterstitialAdManager(prefsHelper)
}