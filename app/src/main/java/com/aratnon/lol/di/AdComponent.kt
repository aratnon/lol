package com.aratnon.lol.di

import com.aratnon.lol.di.scopes.PerActivity
import com.aratnon.lol.di.scopes.PerApplication
import dagger.Component
import dagger.Module
import lol.domain.ad.BaseInterstitial

@PerActivity
@Component(modules = [AdModule::class], dependencies = [AppComponent::class])
interface AdComponent {
    fun getInterstitialAd(): BaseInterstitial
}