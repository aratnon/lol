package com.aratnon.lol

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.content.ContextCompat.startActivity
import io.reactivex.Completable
import io.reactivex.Single
import lol.domain.AppHelper.AppHelper
import android.support.v4.content.ContextCompat.startActivity
import android.net.NetworkInfo
import android.net.ConnectivityManager



class AppHelperImpl constructor(private val context: Context): AppHelper {

    override fun hasInternetConnection(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    override fun getAppVersion(): Single<String>
        = Single.just(BuildConfig.VERSION_NAME)

    private fun createAppLaunchIntent(packageName: String): Intent
    {
        val intent: Intent = try {
            Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName"))
        } catch (exception: ActivityNotFoundException) {
            Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName"))
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        return intent
    }

    override fun showDunidle(): Completable {
        return Completable.fromAction({
            context.startActivity(createAppLaunchIntent("aratnon.pixel.idle.dungeon.dunidle"))
        })
    }

    override fun showReview(): Completable {
        return Completable.fromAction({
            context.startActivity(createAppLaunchIntent(context.packageName))
        })
    }

    override fun showContactMe(): Completable {
        return Completable.fromAction({
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "contact.aratnon@gmail.com", null))
            val chooser = Intent.createChooser(emailIntent, "Contact")
            chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(chooser)
        })
    }
}