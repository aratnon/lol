package com.aratnon.lol.repository

import com.aratnon.lol.R
import io.reactivex.Completable
import io.reactivex.Single
import lol.domain.repository.RuneImageRepository

class RuneImageRepositoryImpl: RuneImageRepository {

    private lateinit var runeImages: HashMap<Int, Int>

    override fun init(): Completable {
        return Completable.fromAction({
            runeImages = HashMap()

            initSorceryRunes()
            initInspirationRunes()
            initPrecisionRunes()
            initDominationRunes()
            initResolveRunes()
        })
    }

    private fun initSorceryRunes()
    {
        runeImages[8200] = R.drawable.rune_8200

//        runeImages[8214] = R.drawable.rune_8214
//        runeImages[8229] = R.drawable.rune_8229
//        runeImages[8230] = R.drawable.rune_8230
//
//        runeImages[8224] = R.drawable.rune_8224
//        runeImages[8226] = R.drawable.rune_8226
//        runeImages[8275] = R.drawable.rune_8275
//
//        runeImages[8210] = R.drawable.rune_8210
//        runeImages[8234] = R.drawable.rune_8234
//        runeImages[8233] = R.drawable.rune_8233
//
//        runeImages[8237] = R.drawable.rune_8237
//        runeImages[8232] = R.drawable.rune_8232
//        runeImages[8236] = R.drawable.rune_8236
    }

    private fun initInspirationRunes()
    {
        runeImages[8300] = R.drawable.rune_8300

//        runeImages[8351] = R.drawable.rune_8351
//        runeImages[8359] = R.drawable.rune_8359
//        runeImages[8360] = R.drawable.rune_8360
//
//        runeImages[8306] = R.drawable.rune_8306
//        runeImages[8304] = R.drawable.rune_8304
//        runeImages[8313] = R.drawable.rune_8313
//
//        runeImages[8321] = R.drawable.rune_8321
//        runeImages[8316] = R.drawable.rune_8316
//        runeImages[8345] = R.drawable.rune_8345
//
//        runeImages[8347] = R.drawable.rune_8347
//        runeImages[8410] = R.drawable.rune_8410
//        runeImages[8352] = R.drawable.rune_8352
    }

    private fun initPrecisionRunes()
    {
        runeImages[8000] = R.drawable.rune_8000

//        runeImages[8005] = R.drawable.rune_8005
//        runeImages[8008] = R.drawable.rune_8008
//        runeImages[8021] = R.drawable.rune_8021
//        runeImages[8010] = R.drawable.rune_8010
//
//        runeImages[9101] = R.drawable.rune_9101
//        runeImages[9111] = R.drawable.rune_9111
//        runeImages[8009] = R.drawable.rune_8009
//
//        runeImages[9104] = R.drawable.rune_9104
//        runeImages[9105] = R.drawable.rune_9105
//        runeImages[9103] = R.drawable.rune_9103
//
//        runeImages[8014] = R.drawable.rune_8014
//        runeImages[8017] = R.drawable.rune_8017
//        runeImages[8299] = R.drawable.rune_8299
    }

    private fun initDominationRunes()
    {
        runeImages[8100] = R.drawable.rune_8100

//        runeImages[8112] = R.drawable.rune_8112
//        runeImages[8124] = R.drawable.rune_8124
//        runeImages[8128] = R.drawable.rune_8128
//        runeImages[9923] = R.drawable.rune_9923
//
//        runeImages[8126] = R.drawable.rune_8126
//        runeImages[8139] = R.drawable.rune_8139
//        runeImages[8143] = R.drawable.rune_8143
//
//        runeImages[8136] = R.drawable.rune_8136
//        runeImages[8120] = R.drawable.rune_8120
//        runeImages[8138] = R.drawable.rune_8138
//
//        runeImages[8135] = R.drawable.rune_8135
//        runeImages[8134] = R.drawable.rune_8134
//        runeImages[8105] = R.drawable.rune_8105
//        runeImages[8106] = R.drawable.rune_8106
    }

    private fun initResolveRunes()
    {
        runeImages[8400] = R.drawable.rune_8400

//        runeImages[8437] = R.drawable.rune_8437
//        runeImages[8439] = R.drawable.rune_8439
//        runeImages[8465] = R.drawable.rune_8465
//
//        runeImages[8446] = R.drawable.rune_8446
//        runeImages[8463] = R.drawable.rune_8463
//        runeImages[8473] = R.drawable.rune_8473
//
//        runeImages[8429] = R.drawable.rune_8429
//        runeImages[8444] = R.drawable.rune_8444
//        runeImages[8472] = R.drawable.rune_8472
//
//        runeImages[8451] = R.drawable.rune_8451
//        runeImages[8453] = R.drawable.rune_8453
//        runeImages[8242] = R.drawable.rune_8242
    }

    override fun getRuneImage(runeId: Int): Int? = runeImages[runeId]
}