package com.aratnon.lol

import android.content.Context
import android.util.DisplayMetrics



class Utility {

    companion object {
        fun calculateNoOfColumns(context: Context?): Int {
            val displayMetrics = context!!.resources.displayMetrics
            val dpWidth = displayMetrics.widthPixels / displayMetrics.density
            return  (dpWidth / 64).toInt()
        }
    }
}