package com.aratnon.lol

import com.google.android.gms.ads.AdRequest

class AdmobRequestUtil {
    companion object {
        fun createAdRequest(): AdRequest
            = AdRequest.Builder()
                .addTestDevice("3BFEE35FC1F1C60CC9D8F132D8429282")
                .build()
    }
}