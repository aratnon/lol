package com.aratnon.lol.ui.item

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.aratnon.lol.App
import com.aratnon.lol.R
import com.aratnon.lol.Utility.Companion.calculateNoOfColumns
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_item_overview.*
import lol.domain.models.ItemData
import lol.presentation.item.overview.ItemOverviewPresenter
import lol.presentation.item.overview.ItemOverviewView
import timber.log.Timber

class ItemOverviewFragment: MvpFragment<ItemOverviewView, ItemOverviewPresenter>(), ItemOverviewView {

    interface ItemOverviewFragmentListener {
        fun onCloseItemOverview()
    }

    companion object {
        private const val ITEM_ID_KEY = "ITEM_ID"

        fun create(itemId: String): ItemOverviewFragment
        {
            val bundle = Bundle()
            bundle.putString(ITEM_ID_KEY, itemId)

            val itemOverviewFragment = ItemOverviewFragment()
            itemOverviewFragment.arguments = bundle

            return itemOverviewFragment
        }
    }

    private lateinit var buildIntoItemAdapter: BuildItemAdapter
    private lateinit var builtFromItemAdapter: BuildItemAdapter
    private var listener: ItemOverviewFragmentListener? = null

    override fun createPresenter(): ItemOverviewPresenter
        = App.appComponent.getItemOverviewPresenter()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is ItemOverviewFragmentListener)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_item_overview, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        item_overview_layout.setOnClickListener({ listener!!.onCloseItemOverview() })

        buildIntoItemAdapter = BuildItemAdapter {  }
        build_into_item_recycler_view.adapter = buildIntoItemAdapter
        build_into_item_recycler_view.layoutManager = GridLayoutManager(context, calculateNoOfColumns(context))

        builtFromItemAdapter = BuildItemAdapter {  }
        built_from_item_recycler_view.adapter = builtFromItemAdapter
        built_from_item_recycler_view.layoutManager = GridLayoutManager(context, calculateNoOfColumns(context))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments.let {
            presenter.loadItem(it!!.getString(ITEM_ID_KEY))
        }
    }

    private fun parseHtml(text: String): Spanned
    {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        }
        else
            Html.fromHtml(text)
    }

    override fun showItemName(itemName: String) {
        item_name_text_view.text = itemName
    }

    override fun showItemGold(gold: String) {
        item_price_text_view.text = gold
    }

    override fun showItemDescription(itemDescription: String) {
        item_description_text_view.text = parseHtml(itemDescription)
    }

    override fun showItemImage(itemImage: String) {
        Glide.with(this)
                .load(itemImage)
                .transition(DrawableTransitionOptions.withCrossFade(150))
                .into(item_image_view)
    }

    override fun showBuildIntoItems(items: MutableList<ItemData>) {
        buildIntoItemAdapter.setItem(items)
    }

    override fun hideBuildIntoItems() {
        build_into_item_layout.visibility = GONE
    }

    override fun showBuiltFromItems(items: MutableList<ItemData>) {
        Timber.d(items.count().toString())
        builtFromItemAdapter.setItem(items)
    }

    override fun hideBuiltFromItems() {
        build_from_item_layout.visibility = GONE
    }
}