package com.aratnon.lol.ui.item

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import com.aratnon.lol.R

class ItemActivity: AppCompatActivity() {

    companion object {

        private const val ITEM_ID_KEY = "ITEM_ID"

        fun start(activity: Activity, championId: String)
        {
            val intent = Intent(activity, ItemActivity::class.java)
            intent.putExtra(ITEM_ID_KEY, championId)
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_home)


//        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        intent.let {
            val itemId = it.getStringExtra(ITEM_ID_KEY)
            val itemOverviewFragment = ItemOverviewFragment.create(itemId)
            supportFragmentManager.beginTransaction()
//                .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down, R.anim.slide_in_up, R.anim.slide_out_down)
                    .add(R.id.activity_item_layout_root, itemOverviewFragment)
                    .commit()
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down)
    }
}