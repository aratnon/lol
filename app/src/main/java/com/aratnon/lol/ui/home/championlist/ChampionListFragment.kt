package com.aratnon.lol.ui.home.championlist

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.App
import com.aratnon.lol.R
import com.aratnon.lol.ui.home.championlist.adapter.ChampionListAdapter
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_champion_list.*
import lol.domain.models.ChampionData
import lol.presentation.champion.list.ChampionListPresenter
import lol.presentation.champion.list.ChampionListView
import timber.log.Timber

class ChampionListFragment: MvpFragment<ChampionListView, ChampionListPresenter>(), ChampionListView {

    interface ChampionListListener
    {
        fun onClickChampion(championId: String)
    }

    companion object {
        const val TAG: String = "CHAMPION_LIST_FRAGMENT"
        fun create(): ChampionListFragment = ChampionListFragment()
    }

    private var championListListener: ChampionListListener? = null

    private lateinit var championListAdapter: ChampionListAdapter

    override fun createPresenter(): ChampionListPresenter
            = App.appComponent.getChampionListPresenter()

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is ChampionListListener) {
            championListListener = context
        } else {
            throw ClassCastException(context.toString() + " must implement ChampionListListener.")
        }
    }

    override fun onDetach() {
        super.onDetach()
        championListListener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_champion_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        championListAdapter = ChampionListAdapter({ championId ->
            presenter.clickChampion(championId)
        })

        champion_list_recycler_view.adapter = championListAdapter
        champion_list_recycler_view.layoutManager = GridLayoutManager(context, 4)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.loadAllChampions()
    }

    override fun showChampionList(championDataList: List<ChampionData>, profileUrl: String) {
        championListAdapter.setProfileUrl(profileUrl)
        championListAdapter.setChampionListData(championDataList)
    }

    override fun showChampionDetail(championId: String) {
        Timber.d(championId)
        championListListener!!.onClickChampion(championId)
    }
}