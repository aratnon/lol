package com.aratnon.lol.ui.champion.skin

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.aratnon.lol.App
import com.aratnon.lol.R
import com.aratnon.lol.ui.champion.abilities.ChampionAbilityFragment
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_champion_skin.*
import lol.domain.models.SkinOfChampion
import lol.presentation.champion.skin.ChampionSkinPresenter
import lol.presentation.champion.skin.ChampionSkinView

class ChampionSkinFragment: MvpFragment<ChampionSkinView, ChampionSkinPresenter>(), ChampionSkinView {

    companion object {
        private const val CHAMPION_ID_KEY = "CHAMPION_ID"

        fun create(championId: String): ChampionSkinFragment {
            val bundle = Bundle()
            bundle.putString(CHAMPION_ID_KEY, championId)

            val championSkinFragment = ChampionSkinFragment()
            championSkinFragment.arguments = bundle

            return championSkinFragment
        }
    }

    private lateinit var championSkinAdapter: ChampionSkinAdapter

    override fun createPresenter(): ChampionSkinPresenter
        = App.appComponent.getChampionSkinPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_champion_skin, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        championSkinAdapter = ChampionSkinAdapter({ skin ->
            presenter.downloadSkin(skin)
        })
        champion_skin_recycler_view.adapter = championSkinAdapter
        champion_skin_recycler_view.layoutManager = LinearLayoutManager(context)
        champion_skin_recycler_view.isNestedScrollingEnabled = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let {
            presenter.loadSkins(it.getString(CHAMPION_ID_KEY))
        }
    }

    override fun showSkins(skins: MutableList<SkinOfChampion>) {
        championSkinAdapter.setSkinUrls(skins)
    }

    override fun showStartDownloadSkin(skinName: String) {
        Toast.makeText(context, getString(R.string.champion_skin_screen_start_download_skin, skinName), Toast.LENGTH_LONG).show()
    }
}