package com.aratnon.lol.ui.home.itemlist

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.App
import com.aratnon.lol.R
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_item_list.*
import lol.domain.models.ItemData
import lol.presentation.item.list.ItemListPresenter
import lol.presentation.item.list.ItemListView

class ItemListFragment: MvpFragment<ItemListView, ItemListPresenter>(), ItemListView {

    companion object {
        fun create(): ItemListFragment = ItemListFragment()
    }

    interface ItemListFragmentListener
    {
        fun onClickItem(itemId: String)
    }

    private lateinit var itemAdapter: ItemAdapter
    private var itemListFragmentListener: ItemListFragmentListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if(context is ItemListFragmentListener)
            itemListFragmentListener = context
    }

    override fun onDetach() {
        super.onDetach()
        itemListFragmentListener = null
    }

    override fun createPresenter(): ItemListPresenter
        = App.appComponent.getItemListPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_item_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        itemAdapter = ItemAdapter { itemId -> presenter.clickItem(itemId.toString()) }
        item_list_recycler_view.layoutManager = LinearLayoutManager(context)
        item_list_recycler_view.adapter = itemAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter.loadItems()
    }

    override fun showItems(items: MutableList<ItemData>) {
        itemAdapter.setItemData(items)
    }

    override fun showItemOverview(itemId: String) {
        itemListFragmentListener!!.onClickItem(itemId)
    }
}