package com.aratnon.lol.ui.home

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.View
import com.aratnon.lol.AdmobRequestUtil
import com.aratnon.lol.App
import com.aratnon.lol.R
import com.aratnon.lol.di.AdComponent
import com.aratnon.lol.di.AdModule
import com.aratnon.lol.di.DaggerAdComponent
import com.aratnon.lol.ui.about.AboutFragment
import com.aratnon.lol.ui.champion.ChampionActivity
import com.aratnon.lol.ui.dunidle.DunidleFragment
import com.aratnon.lol.ui.home.championlist.ChampionListFragment
import com.aratnon.lol.ui.home.itemlist.ItemListFragment
import com.aratnon.lol.ui.item.ItemOverviewFragment
import com.aratnon.lol.ui.rune.list.RuneListFragment
import com.aratnon.lol.ui.rune.overview.RuneOverviewFragment
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_home.*
import lol.presentation.home.HomePresenter
import lol.presentation.home.HomeView


class HomeActivity: MvpActivity<HomeView, HomePresenter>(), HomeView,
        ChampionListFragment.ChampionListListener,
        ItemListFragment.ItemListFragmentListener,
        ItemOverviewFragment.ItemOverviewFragmentListener,
        RuneListFragment.RuneListFragmentListener,
        RuneOverviewFragment.RuneOverviewFragmentListener,
        AboutFragment.AboutFragmentListener {

    private lateinit var championListFragment: ChampionListFragment
    private lateinit var itemListFragment: ItemListFragment
    private lateinit var runeListFragment: RuneListFragment
    private lateinit var aboutFragment: AboutFragment

    private lateinit var bannerAd: AdView

    private val fragments = mutableListOf<Fragment>()

    private lateinit var interstitialAd: InterstitialAd

    private lateinit var adComponent: AdComponent

    companion object {

        fun start(activity: Activity)
        {
            val intent = Intent(activity, HomeActivity::class.java)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        setContentView(R.layout.activity_home)

        if(savedInstanceState == null)
            initFragments()

        initAdComponent()
        initTabLayout()
        initBanner()

    }

    override fun onStart() {
        super.onStart()
        presenter.loadAd()
    }

    override fun createPresenter(): HomePresenter
    {
        return App.appComponent.getHomePresenter()
    }

    private fun initAdComponent()
    {
        adComponent = DaggerAdComponent.builder()
                .adModule(AdModule(this))
                .appComponent(App.appComponent)
                .build()

        App.appComponent.getHomePresenter()
                .setInterstitialAd(adComponent.getInterstitialAd())
    }

    private fun initBanner()
    {
        bannerAd = AdView(this)
        bannerAd.adSize = AdSize.SMART_BANNER
        bannerAd.adUnitId = getString(R.string.banner_ad_id)

        bannerAd.adListener = object: AdListener(){
            override fun onAdLoaded() {
                super.onAdLoaded()
                bannerAd.tag = true
                banner_layout.visibility = View.VISIBLE
            }
        }

        bannerAd.loadAd(AdmobRequestUtil.createAdRequest())
        banner_layout.addView(bannerAd)
    }


    private fun initTabLayout()
    {
        bottom_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when(tab.position){
                    0 -> {
                        showFragment(championListFragment)
                        changeTabIcon(tab, R.drawable.helmet_selected)
                    }

                    1 -> {
                        showFragment(itemListFragment)
                        changeTabIcon(tab, R.drawable.swords_selected)
                    }
                    2 -> {
                        showFragment(runeListFragment)
                        changeTabIcon(tab, R.drawable.icon_rune_selected)
                    }
                    3 -> {
                        showFragment(aboutFragment)
                        changeTabIcon(tab, R.drawable.ic_menu_pink_24dp)
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                when(tab.position){
                    0 -> {
                        changeTabIcon(tab, R.drawable.helmet)
                    }
                    1 -> {
                        changeTabIcon(tab, R.drawable.swords)
                    }
                    2 -> {
                        changeTabIcon(tab, R.drawable.icon_rune)
                    }
                    3 -> {
                        changeTabIcon(tab, R.drawable.ic_menu_white_24dp)
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        changeTabIcon(bottom_tab_layout.getTabAt(0)!!, R.drawable.helmet_selected)
    }

    private fun initFragments()
    {
        aboutFragment = AboutFragment.create()
        createFragment(aboutFragment)

        runeListFragment = RuneListFragment.create()
        createFragment(runeListFragment)

        itemListFragment = ItemListFragment.create()
        createFragment(itemListFragment)

        championListFragment = ChampionListFragment.create()
        createFragment(championListFragment)
    }


    private fun createFragment(fragment: Fragment)
    {
        supportFragmentManager.beginTransaction()
                .add(R.id.home_fragment_parent, fragment)
                .commit()
        fragments.add(fragment)
    }

    private fun changeTabIcon(tab: TabLayout.Tab, resourceId: Int)
    {
        tab.icon = getDrawable(resourceId)
    }

    private fun showFragment(fragment: Fragment)
    {
        supportFragmentManager.beginTransaction()
                .show(fragment)
                .commit()

        hideFragmentExcept(fragment)

//        updateBannerAd(fragment is AboutFragment)
    }

    private fun updateBannerAd(aboutFragmentIsShowing: Boolean)
    {
        if (bannerAd.tag != null && bannerAd.tag is Boolean && bannerAd.tag as Boolean) {
            when(aboutFragmentIsShowing)
            {
                true -> banner_layout.visibility = View.GONE
                else -> banner_layout.visibility = View.VISIBLE
            }
        }
    }

    private fun hideFragmentExcept(exceptFragment: Fragment)
    {
        for (fragment in fragments)
        {
            if(fragment != exceptFragment)
                supportFragmentManager.beginTransaction()
                        .hide(fragment)
                        .commit()
        }
    }

    override fun showChampionDetail(championId: String) {
        ChampionActivity.start(this, championId)
    }

    override fun onClickChampion(championId: String) {
        presenter.clickChampionMaybeShowAd(championId)
    }

    override fun onClickItem(itemId: String) {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .add(R.id.overview_fragment_parent, ItemOverviewFragment.create(itemId))
                .addToBackStack(null)
                .commit()
    }

    override fun onCloseItemOverview() {
        onBackPressed()
    }

    override fun onClickRune(runeId: Int) {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .add(R.id.overview_fragment_parent, RuneOverviewFragment.create(runeId))
                .addToBackStack(null)
                .commit()
    }

    override fun onCloseRuneOverview() {
        onBackPressed()
    }

    override fun onShowDunidleScreen() {
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .add(R.id.overview_fragment_parent, DunidleFragment())
                .addToBackStack(null)
                .commit()
    }
}