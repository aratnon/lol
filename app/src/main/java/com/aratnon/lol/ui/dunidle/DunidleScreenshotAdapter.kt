package com.aratnon.lol.ui.dunidle

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.GlideApp
import com.aratnon.lol.R
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.screenshot_view_holder.view.*

class DunidleScreenshotAdapter: RecyclerView.Adapter<DunidleScreenshotAdapter.ScreenshotViewHolder>() {

    private val screenshots = mutableListOf<Int>()

    init {
        screenshots.add(R.drawable.dunidle_screenshot_1)
        screenshots.add(R.drawable.dunidle_screenshot_2)
        screenshots.add(R.drawable.dunidle_screenshot_3)
        screenshots.add(R.drawable.dunidle_screenshot_4)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScreenshotViewHolder
    {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.screenshot_view_holder, parent, false)
        return ScreenshotViewHolder(view)
    }

    override fun getItemCount(): Int = screenshots.size

    override fun onBindViewHolder(holder: ScreenshotViewHolder, position: Int) {
        holder.bind(screenshots[position])
    }

    inner class ScreenshotViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        fun bind(screenshotId: Int)
        {
            GlideApp.with(itemView)
                    .load(screenshotId)
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .into(itemView.screenshot_image_view)
        }
    }
}