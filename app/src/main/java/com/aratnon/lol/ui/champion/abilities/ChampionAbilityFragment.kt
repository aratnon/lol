package com.aratnon.lol.ui.champion.abilities

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.aratnon.lol.App
import com.aratnon.lol.GlideApp
import com.aratnon.lol.R
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_champion_ability.*
import lol.presentation.champion.abilties.ChampionAbilityPresenter
import lol.presentation.champion.abilties.ChampionAbilityView
import timber.log.Timber

class ChampionAbilityFragment : MvpFragment<ChampionAbilityView, ChampionAbilityPresenter>(), ChampionAbilityView {


    companion object {
        private const val CHAMPION_ID_KEY = "CHAMPION_ID"

        fun create(championId: String): ChampionAbilityFragment{
            val bundle = Bundle()
            bundle.putString(CHAMPION_ID_KEY, championId)

            val championAbilityFragment = ChampionAbilityFragment()
            championAbilityFragment.arguments = bundle

            return championAbilityFragment
        }
    }

    override fun createPresenter(): ChampionAbilityPresenter
        = App.appComponent.getChampionAbilityPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_champion_ability, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let {
            presenter.loadChampionDetail(it.getString(CHAMPION_ID_KEY))
        }
    }

    private fun parseHtml(text: String): Spanned
    {
//        val html = Jsoup.parse(text).toString()
//        return Html.fromHtml(text).toString()
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        }
        else
            Html.fromHtml(text)
    }

    private fun loadImage(imageView: ImageView, url: String)
    {
        context?.let {
            GlideApp.with(it).load(url)
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .into(imageView)
        }
    }

    override fun setChampionName(name: String) {
        champion_name_text_view.text = name
    }

    override fun setChampionTags(tags: String) {
        champion_tags_text_view.text = tags
    }

    override fun setPassiveAbilityName(name: String) {
        passive_ability_name_text_view.text = parseHtml(name)
    }

    override fun setPassiveAbilityDescription(description: String) {
        passive_ability_description_text_view.text = parseHtml(description)
    }

    override fun setPassiveAbilityImage(url: String) {
        loadImage(passive_ability_image_view, url)
    }

    override fun setAbilityQName(name: String) {
        ability_q_name_text_view.text = parseHtml(name)
    }

    override fun setAbilityQInfo(cooldown: String, cost: String, range: String) {
        ability_q_info_text_view.text = getString(R.string.champion_ability_screen_info, cooldown, cost, range)
    }

    override fun setAbilityQDescription(description: String) {
        Timber.d(description)
//        ability_q_webview.loadData(Jsoup.parse(description).toString(), "text/html", "UTF-8")
        ability_q_descripiton_text_view.text = parseHtml(description)
    }

    override fun setAbilityQImage(url: String) {
        loadImage(ability_q_image_view, url)
    }

    override fun setAbilityWName(name: String) {
        ability_w_name_text_view.text = parseHtml(name)
    }

    override fun setAbilityWInfo(cooldown: String, cost: String, range: String) {
        ability_w_info_text_view.text = getString(R.string.champion_ability_screen_info, cooldown, cost, range)
    }

    override fun setAbilityWDescription(description: String) {
        ability_w_descripiton_text_view.text = parseHtml(description)
    }

    override fun setAbilityWImage(url: String) {
        loadImage(ability_w_image_view, url)
    }

    override fun setAbilityEName(name: String) {
        ability_e_name_text_view.text = parseHtml(name)
    }

    override fun setAbilityEInfo(cooldown: String, cost: String, range: String) {
        ability_e_info_text_view.text = getString(R.string.champion_ability_screen_info, cooldown, cost, range)
    }


    override fun setAbilityEDescription(description: String) {
        ability_e_descripiton_text_view.text = parseHtml(description)
    }

    override fun setAbilityEImage(url: String) {
        loadImage(ability_e_image_view, url)
    }

    override fun setAbilityRName(name: String) {
        ability_r_name_text_view.text = parseHtml(name)
    }

    override fun setAbilityRInfo(cooldown: String, cost: String, range: String) {
        ability_r_info_text_view.text = getString(R.string.champion_ability_screen_info, cooldown, cost, range)
    }

    override fun setAbilityRDescription(description: String) {
        ability_r_descripiton_text_view.text = parseHtml(description)
    }

    override fun setAbilityRImage(url: String) {
        loadImage(ability_r_image_view, url)
    }
}