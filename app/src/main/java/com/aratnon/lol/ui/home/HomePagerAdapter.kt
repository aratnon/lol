package com.aratnon.lol.ui.home

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.aratnon.lol.ui.home.championlist.ChampionListFragment
import com.aratnon.lol.ui.home.itemlist.ItemListFragment
import com.aratnon.lol.ui.rune.list.RuneListFragment

class HomePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when(position)
        {
            0 -> ChampionListFragment.create()
            1 -> ItemListFragment.create()
            2 -> RuneListFragment.create()
            else -> ChampionListFragment.create()
        }
    }

    override fun getCount(): Int = 2
}