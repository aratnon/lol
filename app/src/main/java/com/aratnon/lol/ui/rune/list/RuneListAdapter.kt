package com.aratnon.lol.ui.rune.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.GlideApp
import com.aratnon.lol.R
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.view_holder_rune.view.*
import lol.domain.models.RuneData

class RuneListAdapter constructor(private val listener: (Int) -> Unit): RecyclerView.Adapter<RuneListAdapter.RuneListViewHolder>() {

    private var runeData = mutableListOf<RuneData>()

    fun setRuneData(runeData: MutableList<RuneData>)
    {
        this.runeData = runeData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RuneListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder_rune, parent, false)
        return RuneListViewHolder(view)
    }

    override fun getItemCount(): Int = runeData.size

    override fun onBindViewHolder(holder: RuneListViewHolder, position: Int) {
        holder.bind(runeData[position])
    }

    inner class RuneListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        fun bind(runeData: RuneData)
        {
            itemView.rune_name_text_view.text = runeData.name
            itemView.setOnClickListener({ listener(runeData.id) })

            GlideApp.with(itemView)
                    .load(runeData.runeImageUrl)
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .into(itemView.rune_image_view)
        }
    }
}