package com.aratnon.lol.ui.about

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.App
import com.aratnon.lol.BuildConfig
import com.aratnon.lol.R
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_about.*
import lol.presentation.about.AboutPresenter
import lol.presentation.about.AboutView

class AboutFragment: MvpFragment<AboutView, AboutPresenter> (), AboutView {

    private var listener: AboutFragmentListener? = null

    interface AboutFragmentListener {
        fun onShowDunidleScreen()
    }

    companion object {
        fun create(): AboutFragment = AboutFragment()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is AboutFragmentListener)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun createPresenter(): AboutPresenter = App.appComponent.getAboutPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try_dunidle_layout.setOnClickListener({ presenter.onClickDunidle() })
        review_layout.setOnClickListener({ presenter.onClickReview() })
        contact_me_layout.setOnClickListener({ presenter.onClickContactMe() })
        presenter.start()
    }

    override fun setAppVersion(appVersion: String) {
        app_version_text_view.text = getString(R.string.about_screen_version, appVersion)
    }

    override fun setLoLVersion(lolVersion: String) {
        lol_version_text_view.text = getString(R.string.about_screen_lol_version, lolVersion)
    }

    override fun showDunidleScreen() {
        listener!!.onShowDunidleScreen()
    }
}