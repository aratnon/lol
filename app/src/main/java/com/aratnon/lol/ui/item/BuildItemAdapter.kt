package com.aratnon.lol.ui.item

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.GlideApp
import com.aratnon.lol.R
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.view_holder_build_item.view.*
import lol.domain.models.ItemData

class BuildItemAdapter(private val listener: (Int) -> Unit): RecyclerView.Adapter<BuildItemAdapter.BuildItemViewHolder>() {

    private var items = mutableListOf<ItemData>()

    fun setItem(items: MutableList<ItemData>)
    {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BuildItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder_build_item, parent, false)
        return BuildItemViewHolder(view)
    }

    override fun getItemCount() = items.count()

    override fun onBindViewHolder(holder: BuildItemViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class BuildItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        fun bind(itemData: ItemData)
        {
            GlideApp.with(itemView)
                    .load(itemData.imageUrl)
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .into(itemView.item_image_view)
        }
    }
}