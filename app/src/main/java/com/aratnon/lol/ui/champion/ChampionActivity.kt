package com.aratnon.lol.ui.champion

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import com.aratnon.lol.R
import com.aratnon.lol.ui.champion.detail.ChampionDetailFragment

class ChampionActivity: AppCompatActivity() {

    companion object {

        private const val CHAMPION_ID_KEY = "CHAMPION_ID"

        fun start(activity: Activity, championId: String)
        {
            val intent = Intent(activity, ChampionActivity::class.java)
            intent.putExtra(CHAMPION_ID_KEY, championId)
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        requestWindowFeature(Window.FEATURE_NO_TITLE)
//        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_champion)


        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        intent.let {
            val championId = it.getStringExtra(CHAMPION_ID_KEY)
            val championDetailFragment = ChampionDetailFragment.create(championId)
            supportFragmentManager.beginTransaction()
//                .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down, R.anim.slide_in_up, R.anim.slide_out_down)
                    .add(R.id.activity_champion_layout_root, championDetailFragment)
                    .commit()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down)
    }
}