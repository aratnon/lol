package com.aratnon.lol.ui.home.championlist.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.aratnon.lol.GlideApp
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.view_holder_champion_list.view.*
import lol.domain.models.ChampionData

class ChampionListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bind(championData: ChampionData, profileUrl: String, listener: (String) -> Unit)
    {
        itemView.setOnClickListener({
            listener(championData.id.toString())
        })
        GlideApp.with(itemView)
                .load(profileUrl + championData.image.full)
                .transition(DrawableTransitionOptions.withCrossFade(150))
                .into(itemView.champion_profile_image_view)
    }
}