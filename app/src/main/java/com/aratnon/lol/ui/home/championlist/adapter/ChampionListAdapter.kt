package com.aratnon.lol.ui.home.championlist.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.aratnon.lol.R
import lol.domain.models.ChampionData

class ChampionListAdapter(private val listener: (String) -> Unit) : RecyclerView.Adapter<ChampionListViewHolder>() {

    private lateinit var profileUrl: String
    private var championData: List<ChampionData> = emptyList()

    fun setProfileUrl(profileUrl: String)
    {
        this.profileUrl = profileUrl
    }

    fun setChampionListData(championData: List<ChampionData>){
        this.championData = championData
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = championData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChampionListViewHolder
    {
        val view =  LayoutInflater.from(parent.context).inflate(R.layout.view_holder_champion_list, parent, false)
        return ChampionListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChampionListViewHolder, position: Int) {
        holder.bind(championData[position], profileUrl, listener)
    }
}