package com.aratnon.lol.ui.champion.detail.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.aratnon.lol.ui.champion.abilities.ChampionAbilityFragment
import com.aratnon.lol.ui.champion.detail.ChampionOverviewFragment
import com.aratnon.lol.ui.champion.skin.ChampionSkinFragment

class ChampionDetailPagerAdapter(fm: FragmentManager, private val championId: String) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when(position)
        {
            0 -> ChampionOverviewFragment.create(championId)
            1 -> ChampionAbilityFragment.create(championId)
            2 -> ChampionSkinFragment.create(championId)
            else -> ChampionOverviewFragment.create(championId)
        }
    }

    override fun getCount(): Int = 3
}