package com.aratnon.lol.ui.rune.overview

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.App
import com.aratnon.lol.GlideApp
import com.aratnon.lol.R
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_rune_overview.*
import lol.presentation.rune.overview.RuneOverviewPresenter
import lol.presentation.rune.overview.RuneOverviewView

class RuneOverviewFragment: MvpFragment<RuneOverviewView, RuneOverviewPresenter>(), RuneOverviewView {

    interface RuneOverviewFragmentListener
    {
        fun onCloseRuneOverview()
    }

    companion object {
        private const val RUNE_ID_KEY = "RUNE_ID"

        fun create(runeId: Int): RuneOverviewFragment
        {
            val bundle = Bundle()
            bundle.putInt(RUNE_ID_KEY, runeId)

            val runeOverviewFragment = RuneOverviewFragment()
            runeOverviewFragment.arguments = bundle

            return runeOverviewFragment
        }
    }

    private var listener: RuneOverviewFragmentListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is RuneOverviewFragmentListener)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun createPresenter(): RuneOverviewPresenter
        = App.appComponent.getRuneOverviewPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_rune_overview, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rune_overview_layout.setOnClickListener({ listener!!.onCloseRuneOverview() })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments.let {
            presenter.loadRune(it!!.getInt(RUNE_ID_KEY))
        }
    }

    private fun parseHtml(text: String): Spanned
    {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        }
        else
            Html.fromHtml(text)
    }

    override fun setRuneImage(runeImageUrl: String) {
        GlideApp.with(this)
                .load(runeImageUrl)
                .transition(DrawableTransitionOptions.withCrossFade(150))
                .into(rune_image_view)
    }

    override fun setRuneName(runeName: String) {
        rune_name_text_view.text = runeName
    }

    override fun setRuneDescription(runeDescription: String) {
        rune_description_text_view.text = parseHtml(runeDescription)
    }
}