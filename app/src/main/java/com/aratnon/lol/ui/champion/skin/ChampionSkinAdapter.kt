package com.aratnon.lol.ui.champion.skin

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.GlideApp
import com.aratnon.lol.R
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.view_holder_champion_skin.view.*
import lol.domain.models.SkinOfChampion

class ChampionSkinAdapter(private val listener: (SkinOfChampion) -> Unit): RecyclerView.Adapter<ChampionSkinAdapter.ChampionSkinViewHolder>()  {

    private var skins = mutableListOf<SkinOfChampion>()

    fun setSkinUrls(skinUrls: MutableList<SkinOfChampion>)
    {
        this.skins = skinUrls
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChampionSkinViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder_champion_skin, parent, false)
        return ChampionSkinViewHolder(view)
    }

    override fun getItemCount() = skins.count()


    override fun onBindViewHolder(holder: ChampionSkinViewHolder, position: Int) {
        holder.bind(skins[position])
    }


    inner class ChampionSkinViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        fun bind(skin: SkinOfChampion)
        {
            itemView.download_skin_image_view.setOnClickListener({ listener(skin) })

            itemView.skin_name_text_view.text = skin.skinName

            GlideApp.with(itemView)
                    .load(skin.skinUrl)
                    .placeholder(R.drawable.skin_place_holder)
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .into(itemView.champion_skin_image_view)
        }
    }
}