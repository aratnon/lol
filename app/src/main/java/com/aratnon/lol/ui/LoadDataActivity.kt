package com.aratnon.lol.ui

import android.animation.Animator
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import com.aratnon.lol.AbstractAnimatorListener
import com.aratnon.lol.App
import com.aratnon.lol.R
import com.aratnon.lol.ui.home.HomeActivity
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_load_data.*
import lol.presentation.loaddata.LoadDataPresenter
import lol.presentation.loaddata.LoadDataView
import timber.log.Timber

class LoadDataActivity : MvpActivity<LoadDataView, LoadDataPresenter>(), LoadDataView {

    private var shortAnimationTime: Long = 0

    override fun createPresenter(): LoadDataPresenter {
        return App.appComponent.getLoadDataPresenter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(R.layout.activity_load_data)

        shortAnimationTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

        gdpr_agree_button.setOnClickListener({ presenter.onClickAgreeToGDPRConsent() })
        disagree_text_view.setOnClickListener({ presenter.onClickDisagreeToGDPRConsent() })
        try_again_button.setOnClickListener({ presenter.start() })

    }

    override fun onStart() {
        super.onStart()
        Timber.d("onCreate")
        presenter.start()
    }

    override fun resetToDefaultState() {
        wait_load_data_layout.alpha = 0f
        wait_load_data_layout.visibility = View.GONE

        gdpr_consent_layout.alpha = 0f
        gdpr_consent_layout.visibility = View.GONE

        error_layout.visibility = View.GONE
    }

    override fun finishLoadData() {
        HomeActivity.start(this)
        finish()
    }

    override fun setEnabledErrorLoadDataLayout(enabled: Boolean) {
        if(enabled)
        {
            error_layout.visibility = View.VISIBLE
            error_layout.animate()
                    .alpha(1f)
                    .setDuration(shortAnimationTime)
                    .setListener(null)
        }
        else {
            error_layout.animate()
                    .alpha(0f)
                    .setDuration(shortAnimationTime)
                    .setListener(object : AbstractAnimatorListener() {
                        override fun onAnimationEnd(p0: Animator?) {
                            super.onAnimationEnd(p0)
                            gdpr_consent_layout.visibility = View.GONE
                        }
                    })
        }
    }

    override fun setEnabledLoadDataLayout(enabled: Boolean) {
        if(enabled)
        {
            wait_load_data_layout.visibility = View.VISIBLE
            wait_load_data_layout.animate()
                    .alpha(1f)
                    .setDuration(shortAnimationTime)
                    .setListener(null)
            wait_load_data_layout.visibility = View.VISIBLE
        }
        else {
            wait_load_data_layout.animate()
                    .alpha(0f)
                    .setDuration(shortAnimationTime)
                    .setListener(object : AbstractAnimatorListener() {
                        override fun onAnimationEnd(p0: Animator?) {
                            super.onAnimationEnd(p0)
                            wait_load_data_layout.visibility = View.GONE
                        }
                    })
        }
    }

    override fun setEnableConsentLayout(enabled: Boolean) {
        if(enabled)
        {
            gdpr_consent_layout.visibility = View.VISIBLE
            gdpr_consent_layout.animate()
                    .alpha(1f)
                    .setDuration(shortAnimationTime)
                    .setListener(null)
        }
        else {
            gdpr_consent_layout.animate()
                    .alpha(0f)
                    .setDuration(shortAnimationTime)
                    .setListener(object : AbstractAnimatorListener() {
                        override fun onAnimationEnd(p0: Animator?) {
                            super.onAnimationEnd(p0)
                            gdpr_consent_layout.visibility = View.GONE
                        }
                    })
        }
    }
}