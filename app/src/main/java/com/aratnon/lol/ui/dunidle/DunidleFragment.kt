package com.aratnon.lol.ui.dunidle

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.App
import com.aratnon.lol.R
import kotlinx.android.synthetic.main.fragment_dunidle.*
import lol.domain.AppHelper.AppHelper
import StartLinearSnapHelper
import javax.inject.Inject
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SnapHelper



class DunidleFragment: Fragment() {

    @Inject lateinit var  appHelper: AppHelper

    private val dunidleScreenshotAdapter = DunidleScreenshotAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_dunidle, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        google_play_badge_image_view.setOnClickListener({
            appHelper.showDunidle().subscribe()
        })
        close_image_view.setOnClickListener({ activity!!.onBackPressed() })

        App.appComponent.inject(this)

        initRecyclerView()
    }

    private fun initRecyclerView()
    {
        dunidle_screenshot_recycler_view.adapter = dunidleScreenshotAdapter
        dunidle_screenshot_recycler_view.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(dunidle_screenshot_recycler_view)
    }
}