package com.aratnon.lol.ui.champion.detail

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.AdmobRequestUtil
import com.aratnon.lol.App
import com.aratnon.lol.GlideApp
import com.aratnon.lol.R
import com.aratnon.lol.ui.champion.detail.adapter.ChampionDetailPagerAdapter
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import lol.presentation.champion.detail.ChampionDetailPresenter
import lol.presentation.champion.detail.ChampionDetailView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.fragment_champion_detail.*


class ChampionDetailFragment: MvpFragment<ChampionDetailView, ChampionDetailPresenter>(), ChampionDetailView
{
    companion object {
        const val TAG = "CHAMPION_DETAIL_FRAGMENT"
        const val CHAMPION_ID_KEY = "CHAMPION_ID"

        fun create(championId: String): ChampionDetailFragment
        {
            val bundle = Bundle()
            bundle.putString(CHAMPION_ID_KEY, championId)

            val championDetailFragment = ChampionDetailFragment()
            championDetailFragment.arguments = bundle

            return championDetailFragment
        }
    }

    private lateinit var championPagerAdapter: ChampionDetailPagerAdapter

    override fun createPresenter(): ChampionDetailPresenter
            = App.appComponent.getChampionDetailPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_champion_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        close_image_view.setOnClickListener({ presenter.clickClose() })
//        bottom_tab_layout.setupWithViewPager(champion_detail_view_pager)
        bottom_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                presenter.clickTab(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        champion_detail_view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                presenter.scrollToTab(position)
            }
        })
        initBanner()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let { bundle ->
            val championId = bundle.getString(CHAMPION_ID_KEY)
            presenter.loadChampionDetail(championId)

            fragmentManager?.let { fm ->
                championPagerAdapter = ChampionDetailPagerAdapter(fm, championId)
                champion_detail_view_pager.adapter = championPagerAdapter
            }
        }
    }

    private fun initBanner()
    {
        val bannerAd = AdView(context)
        bannerAd.adSize = AdSize.SMART_BANNER
        bannerAd.adUnitId = getString(R.string.banner_ad_id)

        bannerAd.adListener = object: AdListener(){
            override fun onAdLoaded() {
                super.onAdLoaded()
                bannerAd.tag = true
//                banner_layout.visibility = View.VISIBLE
            }
        }

        bannerAd.loadAd(AdmobRequestUtil.createAdRequest())
        banner_layout.addView(bannerAd)
    }

    override fun close() {
        activity?.onBackPressed()
    }

    override fun loadChampionBackground(url: String) {
        context?.let {
            GlideApp.with(it).load(url)
                    .placeholder(R.drawable.vertical_background_place_holder)
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .into(champion_background_image_view)
        }
    }

    override fun loadChampionProfileImage(url: String) {
    }

    override fun changePage(tab: Int) {
        champion_detail_view_pager.currentItem = tab
    }

    override fun changeTab(tab: Int) {
        bottom_tab_layout.getTabAt(tab)?.select()
    }
}