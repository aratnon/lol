package com.aratnon.lol.ui.home.itemlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.GlideApp
import com.aratnon.lol.R
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.view_holder_item.view.*
import lol.domain.models.ItemData

class ItemAdapter constructor(private val listener: (Int) -> Unit): RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    private var itemData = mutableListOf<ItemData>()

    fun setItemData(itemData: MutableList<ItemData>)
    {
        this.itemData = itemData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount() = itemData.count()

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(itemData[position])
    }

    inner class ItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        fun bind(itemData: ItemData)
        {
            itemView.setOnClickListener({ listener(itemData.id) })
            itemView.item_name_text_view.text = itemData.name
            itemView.item_price_text_view.text = itemData.getGoldString()

            GlideApp.with(itemView)
                    .load(itemData.imageUrl)
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .into(itemView.item_image_view)
        }
    }
}