package com.aratnon.lol.ui.rune.list

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.App
import com.aratnon.lol.R
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_rune_list.*
import lol.domain.models.RuneConstant
import lol.domain.models.RuneData
import lol.presentation.rune.list.RuneListPresenter
import lol.presentation.rune.list.RuneListView

class RuneListFragment: MvpFragment<RuneListView, RuneListPresenter>(), RuneListView {

    companion object {
        fun create(): RuneListFragment
        {
            return RuneListFragment()
        }
    }

    interface RuneListFragmentListener {
        fun onClickRune(runeId: Int)
    }

    private lateinit var keystoneAdapter: RuneListAdapter
    private lateinit var firstRuneSlotAdapter: RuneListAdapter
    private lateinit var secondRuneSlotAdapter: RuneListAdapter
    private lateinit var thirdRuneSlotAdapter: RuneListAdapter
    private var listener: RuneListFragmentListener? = null

    override fun createPresenter(): RuneListPresenter
            = App.appComponent.getRuneListPresenter()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is RuneListFragmentListener)
            listener = context
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
        = inflater.inflate(R.layout.fragment_rune_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rune_tab_layout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab!!.position)
                {
                    0 -> presenter.loadRune(RuneConstant.CATEGORY_PRECISION)
                    1 -> presenter.loadRune(RuneConstant.CATEGORY_DOMINATION)
                    2 -> presenter.loadRune(RuneConstant.CATEGORY_SORCERY)
                    3 -> presenter.loadRune(RuneConstant.CATEGORY_INSPIRATION)
                    4 -> presenter.loadRune(RuneConstant.CATEGORY_RESOLVE)
                }
            }
        })

//        rune_precision_image_view.setOnClickListener({ presenter.loadRune(RuneConstant.CATEGORY_PRECISION) })
//        rune_domination_image_view.setOnClickListener({ presenter.loadRune(RuneConstant.CATEGORY_DOMINATION) })
//        rune_sorcery_image_view.setOnClickListener({ presenter.loadRune(RuneConstant.CATEGORY_SORCERY) })
//        rune_inspiration_image_view.setOnClickListener({ presenter.loadRune(RuneConstant.CATEGORY_INSPIRATION) })
//        rune_resolve_image_view.setOnClickListener({ presenter.loadRune(RuneConstant.CATEGORY_RESOLVE) })

        keystoneAdapter = RuneListAdapter { runeId -> presenter.clickRune(runeId)}
        setupRecyclerView(key_stone_recycler_view, keystoneAdapter)

        firstRuneSlotAdapter = RuneListAdapter { runeId -> presenter.clickRune(runeId)}
        setupRecyclerView(rune_slot_1_recycler_view, firstRuneSlotAdapter)

        secondRuneSlotAdapter = RuneListAdapter { runeId -> presenter.clickRune(runeId)}
        setupRecyclerView(rune_slot_2_recycler_view, secondRuneSlotAdapter)

        thirdRuneSlotAdapter = RuneListAdapter { runeId -> presenter.clickRune(runeId)}
        setupRecyclerView(rune_slot_3_recycler_view, thirdRuneSlotAdapter)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView, runeListAdapter: RuneListAdapter)
    {
        recyclerView.adapter = runeListAdapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.isNestedScrollingEnabled = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.loadRune(RuneConstant.CATEGORY_PRECISION)
    }

    override fun showRuneOverview(runeId: Int) {
        listener!!.onClickRune(runeId)
    }

    override fun showRuneCategoryName(runeCategoryName: String) {
//        rune_category_name_text_view.text = runeCategoryName
    }

    override fun showRuneCategoryImage(imageId: Int) {
//        GlideApp.with(this)
//                .load(imageId)
//                .into(rune_image_view)
    }

    override fun showKeystones(runeData: MutableList<RuneData>) {
        keystoneAdapter.setRuneData(runeData)
    }

    override fun showRunePrecision() {
        rune_slot_1_text_view.text = getString(R.string.rune_list_screen_heroism)
        rune_slot_2_text_view.text = getString(R.string.rune_list_screen_legend)
        rune_slot_3_text_view.text = getString(R.string.rune_list_screen_key_combat)
    }

    override fun showRuneDomination() {
        rune_slot_1_text_view.text = getString(R.string.rune_list_screen_malice)
        rune_slot_2_text_view.text = getString(R.string.rune_list_screen_tracking)
        rune_slot_3_text_view.text = getString(R.string.rune_list_screen_hunter)
    }

    override fun showRuneSorcery() {
        rune_slot_1_text_view.text = getString(R.string.rune_list_screen_artifact)
        rune_slot_2_text_view.text = getString(R.string.rune_list_screen_excellence)
        rune_slot_3_text_view.text = getString(R.string.rune_list_screen_power)
    }

    override fun showRuneResolve() {
        rune_slot_1_text_view.text = getString(R.string.rune_list_screen_strength)
        rune_slot_2_text_view.text = getString(R.string.rune_list_screen_resistance)
        rune_slot_3_text_view.text = getString(R.string.rune_list_screen_vitality)
    }

    override fun showRuneInspiration() {
        rune_slot_1_text_view.text = getString(R.string.rune_list_screen_contraption)
        rune_slot_2_text_view.text = getString(R.string.rune_list_screen_tomorrow)
        rune_slot_3_text_view.text = getString(R.string.rune_list_screen_beyond)
    }

    override fun showFirstRuneSlot(runeData: MutableList<RuneData>) {
        firstRuneSlotAdapter.setRuneData(runeData)
    }

    override fun showSecondRuneSlot(runeData: MutableList<RuneData>) {
        secondRuneSlotAdapter.setRuneData(runeData)
    }

    override fun showThirdRuneSlot(runeData: MutableList<RuneData>) {
        thirdRuneSlotAdapter.setRuneData(runeData)
    }
}