package com.aratnon.lol.ui.champion.detail

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aratnon.lol.App
import com.aratnon.lol.R
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import kotlinx.android.synthetic.main.fragment_chmpion_overview.*
import lol.presentation.champion.overview.ChampionOverviewPresenter
import lol.presentation.champion.overview.ChampionOverviewView
import com.aratnon.lol.GlideApp
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.natasa.progressviews.LineProgressBar


class ChampionOverviewFragment: MvpFragment<ChampionOverviewView, ChampionOverviewPresenter>(), ChampionOverviewView {

    companion object {
        const val TAG = "CHAMPION_OVERVIEW_FRAGMENT"
        private const val CHAMPION_ID_KEY = "CHAMPION_ID"

        fun create(championId: String): ChampionOverviewFragment
        {
            val bundle = Bundle()
            bundle.putString(CHAMPION_ID_KEY, championId)

            val championOverviewFragment = ChampionOverviewFragment()
            championOverviewFragment.arguments = bundle

            return championOverviewFragment
        }
    }

    override fun createPresenter(): ChampionOverviewPresenter
            = App.appComponent.getChampionOverviewPresenter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_chmpion_overview, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        attack_progress_bar.setRoundEdgeProgress(true)
        defense_progress_bar.setRoundEdgeProgress(true)
        magic_progress_bar.setRoundEdgeProgress(true)
        difficulty_progress_bar.setRoundEdgeProgress(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let {
            val championId = it.getString(CHAMPION_ID_KEY)
            presenter.loadChampionDetail(championId)
        }
    }

    override fun loadChampionProfileImage(url: String) {
//        context?.let {
//            GlideApp.with(it).load(url)
//                    .transition(DrawableTransitionOptions.withCrossFade(50))
//                    .into(champion_profile_image_view)
//        }
    }

    override fun setChampionName(name: String) {
        champion_name_text_view.text = name
    }

    override fun setChampionTags(tags: String) {
        champion_tags_text_view.text = tags
    }

    private fun animateProgressBar(progressBar: LineProgressBar, value: Float)
    {
        val progressAnimator = ObjectAnimator.ofFloat(progressBar, "progress", 0.0f, value)
        progressAnimator.duration = 1000
        progressAnimator.start()
    }

    override fun setAttackBar(attack: Float) = animateProgressBar(attack_progress_bar, attack)

    override fun setDefenseBar(defense: Float) = animateProgressBar(defense_progress_bar, defense)

    override fun setMagicBar(magic: Float) = animateProgressBar(magic_progress_bar, magic)

    override fun setDifficultyBar(difficulty: Float) = animateProgressBar(difficulty_progress_bar, difficulty)

    override fun setHealth(healthText: String) {
        health_text_view.text = getString(R.string.champion_overview_screen_health, healthText)
    }

    override fun setHealthRegen(healthRegenText: String) {
        health_regen_text_view.text = getString(R.string.champion_overview_screen_health_regen, healthRegenText)
    }

    override fun setMana(manaText: String) {
        mana_text_view.text = getString(R.string.champion_overview_screen_mana, manaText)
    }

    override fun setManaRegen(manaRegenText: String) {
        mana_regen_text_view.text = getString(R.string.champion_overview_screen_mana_regen, manaRegenText)
    }

    override fun setAttack(attackText: String) {
        attack_damage_text_view.text = getString(R.string.champion_overview_screen_attack_damage, attackText)
    }

    override fun setAttackSpeed(attackSpeedText: String) {
        attack_speed_text_view.text = getString(R.string.champion_overview_screen_attack_speed, attackSpeedText)
    }

    override fun setArmor(armorText: String) {
        armor_text_view.text = getString(R.string.champion_overview_screen_armor, armorText)
    }

    override fun setMagicResist(magicResistText: String) {
        magic_resist_text_view.text = getString(R.string.champion_overview_screen_magic_resist, magicResistText)
    }

    override fun setRange(rangeText: String) {
        range_text_view.text = getString(R.string.champion_overview_screen_range, rangeText)
    }

    override fun setMoveSpeed(moveSpeedText: String) {
        movement_text_view.text = getString(R.string.champion_overview_screen_move_speed, moveSpeedText)
    }

    override fun setChampionLore(lore: String) {
        champion_lore_text_view.text = lore
    }
}