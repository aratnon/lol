package lol.presentation.about

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.about.AboutUseCase
import lol.domain.interactor.champion.ChampionDetailUseCase
import javax.inject.Inject

class AboutPresenter @Inject constructor(private val aboutUseCase: AboutUseCase): MvpBasePresenter<AboutView>() {

    fun start()
    {
        loadAppVersion()
        loadLoLVersion()
    }

    private fun loadAppVersion(){
        ifViewAttached { view ->
            aboutUseCase.getAppVersion()
                    .subscribe({ version ->
                        view.setAppVersion(version)
                    })
        }
    }

    private fun loadLoLVersion(){
        ifViewAttached { view ->
            aboutUseCase.getLoLVersion()
                    .subscribe({ version ->
                        view.setLoLVersion(version)
                    })
        }
    }

    fun onClickDunidle()
    {
        ifViewAttached { view ->
            view.showDunidleScreen()
        }
    }

    fun onClickReview()
    {
        ifViewAttached {
            aboutUseCase.showReview()
                    .subscribe()
        }
    }

    fun onClickContactMe()
    {
        ifViewAttached {
            aboutUseCase.showContactMe()
                    .subscribe()
        }
    }
}