package lol.presentation.about

import com.hannesdorfmann.mosby3.mvp.MvpView

interface AboutView: MvpView {
    fun setAppVersion(appVersion: String)
    fun setLoLVersion(lolVersion: String)
    fun showDunidleScreen()
}