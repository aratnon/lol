package lol.presentation.home

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.ad.BaseInterstitial
import lol.domain.ad.InterstitialAdManager
import javax.inject.Inject

class HomePresenter @Inject constructor(private val interstitialAdManager: InterstitialAdManager)
    : MvpBasePresenter<HomeView>() {

    fun setInterstitialAd(interstitial: BaseInterstitial)
    {
        interstitialAdManager.setInterstitialAd(interstitial)
    }

    fun loadAd()
    {
        interstitialAdManager.isLoaded()
                .subscribe({ isLoaded ->
                    if(!isLoaded)
                        interstitialAdManager.load()
                })
    }

    fun clickChampionMaybeShowAd(championId: String)
    {
        interstitialAdManager.canShow()
                .subscribe({ canShow ->
                    when(canShow)
                    {
                        true -> {
                            interstitialAdManager.show(object: BaseInterstitial.InterstitialAdListener
                            {
                                override fun onAdClosed() {
                                    ifViewAttached { view ->  view.showChampionDetail(championId)}
                                }
                            })
                        }
                        false -> {
                            ifViewAttached { view ->  view.showChampionDetail(championId)}
                        }
                    }
                })

        interstitialAdManager.increaseAdCount()
    }
}