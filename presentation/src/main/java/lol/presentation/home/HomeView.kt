package lol.presentation.home

import com.hannesdorfmann.mosby3.mvp.MvpView

interface HomeView: MvpView {
    fun showChampionDetail(championId: String)
}