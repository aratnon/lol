package lol.presentation.rune.overview

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.rune.GetRuneUseCase
import javax.inject.Inject

class RuneOverviewPresenter @Inject constructor(private val getRuneUseCase: GetRuneUseCase): MvpBasePresenter<RuneOverviewView>() {

    fun loadRune(runeId: Int)
    {
        ifViewAttached { view ->
            getRuneUseCase.execute(runeId)
                    .subscribe({ rune ->
                        view.setRuneName(rune.name)
                        view.setRuneImage(rune.runeImageUrl)
                        view.setRuneDescription(rune.longDesc)
                    })
        }
    }
}