package lol.presentation.rune.overview

import com.hannesdorfmann.mosby3.mvp.MvpView

interface RuneOverviewView: MvpView {

    fun setRuneImage(runeImageUrl: String)
    fun setRuneName(runeName: String)
    fun setRuneDescription(runeDescription: String)
}