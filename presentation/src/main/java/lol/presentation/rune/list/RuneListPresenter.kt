package lol.presentation.rune.list

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.rune.GetRuneCategoryUseCase
import lol.domain.interactor.rune.GetRuneListByCategoryUseCase
import lol.domain.models.RuneConstant
import javax.inject.Inject

class RuneListPresenter @Inject constructor(private val getRuneListByCategoryUseCase: GetRuneListByCategoryUseCase)
    : MvpBasePresenter<RuneListView>(){

    fun loadRune(category: String)
    {
        ifViewAttached { view ->

            when(category)
            {
                RuneConstant.CATEGORY_PRECISION -> view.showRunePrecision()
                RuneConstant.CATEGORY_DOMINATION -> view.showRuneDomination()
                RuneConstant.CATEGORY_SORCERY -> view.showRuneSorcery()
                RuneConstant.CATEGORY_RESOLVE -> view.showRuneResolve()
                RuneConstant.CATEGORY_INSPIRATION -> view.showRuneInspiration()
            }

            getRuneListByCategoryUseCase.execute(category)
                    .subscribe({ runeDataByCategory ->
                        view.showKeystones(runeDataByCategory[0])
                        view.showFirstRuneSlot(runeDataByCategory[1])
                        view.showSecondRuneSlot(runeDataByCategory[2])
                        view.showThirdRuneSlot(runeDataByCategory[3])
                    })
        }
    }

    fun clickRune(runeId: Int)
    {
        ifViewAttached { view -> view.showRuneOverview(runeId) }
    }
}