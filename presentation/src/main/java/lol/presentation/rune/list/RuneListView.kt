package lol.presentation.rune.list

import com.hannesdorfmann.mosby3.mvp.MvpView
import lol.domain.models.RuneData

interface RuneListView: MvpView
{
    fun showRuneOverview(runeId: Int)

    fun showRuneCategoryName(runeCategoryName: String)
    fun showRuneCategoryImage(imageId: Int)

    fun showRunePrecision()
    fun showRuneDomination()
    fun showRuneSorcery()
    fun showRuneResolve()
    fun showRuneInspiration()

    fun showKeystones(runeData: MutableList<RuneData>)
    fun showFirstRuneSlot(runeData: MutableList<RuneData>)
    fun showSecondRuneSlot(runeData: MutableList<RuneData>)
    fun showThirdRuneSlot(runeData: MutableList<RuneData>)
}