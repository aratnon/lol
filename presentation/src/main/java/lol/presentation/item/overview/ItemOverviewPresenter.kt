package lol.presentation.item.overview

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.item.GetMultipleItemsUseCase
import lol.domain.interactor.item.GetSingleItemUseCase
import lol.domain.models.ItemData
import javax.inject.Inject


class ItemOverviewPresenter @Inject constructor(private val getSingleItemUseCase: GetSingleItemUseCase,
                                                private val getMultipleItemsUseCase: GetMultipleItemsUseCase): MvpBasePresenter<ItemOverviewView>() {

    fun loadItem(itemId: String)
    {
        ifViewAttached { view ->
            getSingleItemUseCase.execute(itemId)
                    .subscribe({ item ->
                        loadBuildIntoItems(item)
                        loadBuiltFromItems(item)

                        view.showItemName(item.name)
                        view.showItemGold(item.getGoldString())
                        view.showItemImage(item.imageUrl)
                        view.showItemDescription(item.description)
                    })
        }
    }

    private fun loadBuildIntoItems(itemData: ItemData)
    {
        ifViewAttached { view ->
            if(itemData.into != null)
            {
                getMultipleItemsUseCase.execute(itemData.into)
                        .subscribe({ items ->
                            view.showBuildIntoItems(items)
                        })
            }
            else view.hideBuildIntoItems()
        }
    }

    private fun loadBuiltFromItems(itemData: ItemData)
    {
        ifViewAttached { view ->
            if(itemData.from != null)
            {
                getMultipleItemsUseCase.execute(itemData.from)
                        .subscribe({ items ->
                            view.showBuiltFromItems(items)
                        })
            }
            else view.hideBuiltFromItems()
        }
    }
}