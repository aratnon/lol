package lol.presentation.item.overview

import com.hannesdorfmann.mosby3.mvp.MvpView
import lol.domain.models.ItemData

interface ItemOverviewView: MvpView {
    fun showItemName(itemName: String)
    fun showItemGold(gold: String)
    fun showItemImage(itemImage: String)
    fun showItemDescription(itemDescription: String)
    fun showBuildIntoItems(items: MutableList<ItemData>)
    fun hideBuildIntoItems()
    fun showBuiltFromItems(items: MutableList<ItemData>)
    fun hideBuiltFromItems()
}