package lol.presentation.item.list

import com.hannesdorfmann.mosby3.mvp.MvpView
import lol.domain.models.ItemData

interface ItemListView: MvpView
{
    fun showItems(items: MutableList<ItemData>)
    fun showItemOverview(itemId: String)
}