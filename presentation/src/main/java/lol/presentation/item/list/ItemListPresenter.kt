package lol.presentation.item.list

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.item.GetItemListUseCase
import javax.inject.Inject

class ItemListPresenter @Inject constructor(private val getItemListUseCase: GetItemListUseCase)
    : MvpBasePresenter<ItemListView>() {

    fun loadItems()
    {
        ifViewAttached { view ->
            getItemListUseCase.execute()
                    .subscribe({ items ->
                        view.showItems(items)
                    })
        }
    }

    fun clickItem(itemId: String)
    {
        ifViewAttached { view -> view.showItemOverview(itemId) }
    }
}