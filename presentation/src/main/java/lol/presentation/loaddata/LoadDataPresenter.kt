package lol.presentation.loaddata
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import io.reactivex.disposables.CompositeDisposable
import lol.domain.interactor.gdpr.GDPRUseCase
import lol.domain.interactor.loaddata.LoadDataUseCase
import javax.inject.Inject

class LoadDataPresenter @Inject constructor(private val loadDataUseCase: LoadDataUseCase,
                                            private val gdprUseCase: GDPRUseCase): MvpBasePresenter<LoadDataView>() {

    private val compositeDisposable = CompositeDisposable()

    fun start()
    {
        ifViewAttached { view ->
            view.resetToDefaultState()

            if(gdprUseCase.hasConsent())
            {
                view.setEnabledLoadDataLayout(true)
                view.setEnableConsentLayout(false)

                val disposable = loadDataUseCase.loadData()
                        .subscribe({
                            view.setEnabledLoadDataLayout(false)
                            view.finishLoadData()
                        }, { error ->
                            error.printStackTrace()
                            view.setEnabledLoadDataLayout(false)
                            view.setEnableConsentLayout(false)
                            view.setEnabledErrorLoadDataLayout(true)
                        })
                compositeDisposable.add(disposable)
            }
            else
            {
                view.setEnabledLoadDataLayout(false)
                view.setEnableConsentLayout(true)
            }
        }
    }

    fun onClickAgreeToGDPRConsent()
    {
        ifViewAttached {
            gdprUseCase.saveConsent(true)
                    .subscribe({
                        start()
                    })
        }
    }

    fun onClickDisagreeToGDPRConsent()
    {
        ifViewAttached {
            gdprUseCase.saveConsent(false)
                    .subscribe({
                        start()
                    })
        }
    }


    fun testChampion()
    {
        val disposable = loadDataUseCase.getChampion()
                .subscribe()
        compositeDisposable.add(disposable)
    }
}