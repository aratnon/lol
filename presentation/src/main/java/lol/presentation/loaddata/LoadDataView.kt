package lol.presentation.loaddata

import com.hannesdorfmann.mosby3.mvp.MvpView

interface LoadDataView : MvpView {

    fun resetToDefaultState()
    fun finishLoadData()
    fun setEnabledErrorLoadDataLayout(enabled: Boolean)
    fun setEnabledLoadDataLayout(enabled: Boolean)
    fun setEnableConsentLayout(enabled: Boolean)

}