package lol.presentation.champion.abilties

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.champion.ChampionDetailUseCase
import javax.inject.Inject

class ChampionAbilityPresenter @Inject constructor(private val championDetailUseCase: ChampionDetailUseCase) : MvpBasePresenter<ChampionAbilityView>() {

    fun loadChampionDetail(championId: String)
    {
        ifViewAttached { view ->
            championDetailUseCase.getChampion(championId)
                    .subscribe({ championData ->

                        view.setChampionName(championDetailUseCase.getChampionName(championData.name, championData.title))
                        view.setChampionTags(championDetailUseCase.getChampionTags(championData.tags))

                        val passive = championData.passive
                        view.setPassiveAbilityName(passive.name)
                        view.setPassiveAbilityDescription(passive.description)
                        view.setPassiveAbilityImage(championDetailUseCase.getPassiveImageUrl(passive.image.full))

                        val spells = championData.spells
                        view.setAbilityQName(spells[0].name!!)
                        view.setAbilityQInfo(championDetailUseCase.getAbilityCooldownText(spells[0]),
                                championDetailUseCase.getAbilityCostText(spells[0]),
                                championDetailUseCase.getAbilityRangeText(spells[0]))
                        view.setAbilityQDescription(championDetailUseCase.getAbilityDescription(spells[0]))
                        view.setAbilityQImage(championDetailUseCase.getAbilityImageUrl(spells[0].image.full))

                        view.setAbilityWName(spells[1].name!!)
                        view.setAbilityWInfo(championDetailUseCase.getAbilityCooldownText(spells[1]),
                                championDetailUseCase.getAbilityCostText(spells[1]),
                                championDetailUseCase.getAbilityRangeText(spells[1]))
                        view.setAbilityWDescription(championDetailUseCase.getAbilityDescription(spells[1]))
                        view.setAbilityWImage(championDetailUseCase.getAbilityImageUrl(spells[1].image.full))

                        view.setAbilityEName(spells[2].name!!)
                        view.setAbilityEInfo(championDetailUseCase.getAbilityCooldownText(spells[2]),
                                championDetailUseCase.getAbilityCostText(spells[2]),
                                championDetailUseCase.getAbilityRangeText(spells[2]))
                        view.setAbilityEDescription(championDetailUseCase.getAbilityDescription(spells[2]))
                        view.setAbilityEImage(championDetailUseCase.getAbilityImageUrl(spells[2].image.full))

                        view.setAbilityRName(spells[3].name!!)
                        view.setAbilityRInfo(championDetailUseCase.getAbilityCooldownText(spells[3]),
                                championDetailUseCase.getAbilityCostText(spells[3]),
                                championDetailUseCase.getAbilityRangeText(spells[3]))
                        view.setAbilityRDescription(championDetailUseCase.getAbilityDescription(spells[3]))
                        view.setAbilityRImage(championDetailUseCase.getAbilityImageUrl(spells[3].image.full))
                    }, { error ->
                        error.printStackTrace()
                    })
        }
    }

}