package lol.presentation.champion.abilties

import com.hannesdorfmann.mosby3.mvp.MvpView

interface ChampionAbilityView : MvpView {

    fun setChampionName(name: String)
    fun setChampionTags(tags: String)

    fun setPassiveAbilityName(name: String)
    fun setPassiveAbilityDescription(description: String)
    fun setPassiveAbilityImage(url: String)

    fun setAbilityQName(name: String)
    fun setAbilityQInfo(cooldown: String, cost: String, range: String)
    fun setAbilityQDescription(description: String)
    fun setAbilityQImage(url: String)

    fun setAbilityWName(name: String)
    fun setAbilityWInfo(cooldown: String, cost: String, range: String)
    fun setAbilityWDescription(description: String)
    fun setAbilityWImage(url: String)

    fun setAbilityEName(name: String)
    fun setAbilityEInfo(cooldown: String, cost: String, range: String)
    fun setAbilityEDescription(description: String)
    fun setAbilityEImage(url: String)

    fun setAbilityRName(name: String)
    fun setAbilityRInfo(cooldown: String, cost: String, range: String)
    fun setAbilityRDescription(description: String)
    fun setAbilityRImage(url: String)

}