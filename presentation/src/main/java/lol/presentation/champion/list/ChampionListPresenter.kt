package lol.presentation.champion.list

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.champion.ChampionListUseCase
import timber.log.Timber
import javax.inject.Inject

class ChampionListPresenter @Inject constructor(private val championListUseCase: ChampionListUseCase)
    : MvpBasePresenter<ChampionListView>()
{
    fun loadAllChampions(){
        ifViewAttached { view ->
            championListUseCase.getAllChampions()
                    .subscribe({ championListData ->
                        view.showChampionList(championListData, championListUseCase.getProfileUrl())

                        for( data in championListData) if(data.spells[0].name == null) Timber.d(data.id.toString())
                    }, { error ->
                        error.printStackTrace()
                    })
        }
    }

    fun clickChampion(championId: String)
    {
        ifViewAttached { view -> view.showChampionDetail(championId) }
    }
}
