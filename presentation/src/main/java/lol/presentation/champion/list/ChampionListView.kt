package lol.presentation.champion.list

import com.hannesdorfmann.mosby3.mvp.MvpView
import lol.domain.models.ChampionData

interface ChampionListView: MvpView {
    fun showChampionList(championDataList: List<ChampionData>, profileUrl: String)
    fun showChampionDetail(championId: String)
}