package lol.presentation.champion.overview

import com.hannesdorfmann.mosby3.mvp.MvpView

interface ChampionOverviewView : MvpView {

    fun loadChampionProfileImage(url: String)
    fun setChampionName(name: String)
    fun setChampionTags(tags: String)

    fun setAttackBar(attack: Float)
    fun setDefenseBar(defense: Float)
    fun setMagicBar(magic: Float)
    fun setDifficultyBar(difficulty: Float)

    fun setHealth(healthText: String)
    fun setHealthRegen(healthRegenText: String)
    fun setMana(manaText: String)
    fun setManaRegen(manaRegenText: String)
    fun setAttack(attackText: String)
    fun setAttackSpeed(attackSpeedText: String)
    fun setArmor(armorText: String)
    fun setMagicResist(magicResistText: String)
    fun setRange(rangeText: String)
    fun setMoveSpeed(moveSpeedText: String)

    fun setChampionLore(lore: String)

}