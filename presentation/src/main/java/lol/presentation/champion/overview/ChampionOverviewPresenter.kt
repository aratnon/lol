package lol.presentation.champion.overview

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.champion.ChampionDetailUseCase
import javax.inject.Inject

class ChampionOverviewPresenter @Inject constructor(private val championDetailUseCase: ChampionDetailUseCase)
    : MvpBasePresenter<ChampionOverviewView>()
{
    fun loadChampionDetail(championId: String)
    {
        ifViewAttached { view ->
            championDetailUseCase.getChampion(championId)
                    .subscribe({ championData ->
                        view.loadChampionProfileImage(championDetailUseCase.getChampionProfileUrl(championData.key))

                        view.setChampionName(championDetailUseCase.getChampionName(championData.name, championData.title))
                        view.setChampionTags(championDetailUseCase.getChampionTags(championData.tags))

                        view.setAttackBar(championDetailUseCase.getChampionInfoValue(championData.info.attack))
                        view.setDefenseBar(championDetailUseCase.getChampionInfoValue(championData.info.defense))
                        view.setMagicBar(championDetailUseCase.getChampionInfoValue(championData.info.magic))
                        view.setDifficultyBar(championDetailUseCase.getChampionInfoValue(championData.info.difficulty))

                        view.setHealth(championDetailUseCase.getStatString(championData.stats.hp, championData.stats.hpperlevel))
                        view.setHealthRegen(championDetailUseCase.getStatString(championData.stats.hpregen, championData.stats.hpregenperlevel))
                        view.setMana(championDetailUseCase.getStatString(championData.stats.mp, championData.stats.mpperlevel))
                        view.setManaRegen(championDetailUseCase.getStatString(championData.stats.mpregen, championData.stats.mpregen))
                        view.setAttack(championDetailUseCase.getStatString(championData.stats.attackdamage, championData.stats.attackdamageperlevel))
                        view.setAttackSpeed(championDetailUseCase.getStatString(championData.stats.attackspeedoffset, championData.stats.attackdamageperlevel))
                        view.setArmor(championDetailUseCase.getStatString(championData.stats.armor, championData.stats.armorperlevel))
                        view.setMagicResist(championDetailUseCase.getStatString(championData.stats.spellblock, championData.stats.spellblockperlevel))
                        view.setRange(championData.stats.attackrange.toString())
                        view.setMoveSpeed(championData.stats.movespeed.toString())

                        view.setChampionLore(championData.lore)
                    })
        }
    }
}
