package lol.presentation.champion.detail

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.champion.ChampionDetailUseCase
import javax.inject.Inject

class ChampionDetailPresenter @Inject constructor(private val championDetailUseCase: ChampionDetailUseCase)
    : MvpBasePresenter<ChampionDetailView>()
{
    fun loadChampionDetail(championId: String)
    {
        ifViewAttached { view ->
            championDetailUseCase.getChampion(championId)
                    .subscribe({ championData ->
                        view.loadChampionBackground(championDetailUseCase.getChampionLoadingBackgroundUrl(championData.key))
                        view.loadChampionProfileImage(championDetailUseCase.getChampionProfileUrl(championData.key))
//                        view.setChampionName(championDetailUseCase.getChampionName(championData.name, championData.title))
//                        view.setChampionTags(championDetailUseCase.getChampionTags(championData.tags))
                    })
        }
    }

    fun clickClose() {
        ifViewAttached { view ->
            view.close()
        }
    }

    fun clickTab(tabPosition: Int)
    {
        ifViewAttached { view ->
            view.changePage(tabPosition)
        }
    }

    fun scrollToTab(tabPosition: Int)
    {
        ifViewAttached { view ->
            view.changeTab(tabPosition)
        }
    }
}
