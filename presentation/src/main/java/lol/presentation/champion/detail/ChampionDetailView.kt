package lol.presentation.champion.detail

import com.hannesdorfmann.mosby3.mvp.MvpView

interface ChampionDetailView : MvpView {

    fun loadChampionBackground(url: String)
    fun loadChampionProfileImage(url: String)
    fun close()

    fun changePage(tab: Int)
    fun changeTab(tab: Int)


}