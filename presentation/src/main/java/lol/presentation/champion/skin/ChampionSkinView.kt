package lol.presentation.champion.skin

import com.hannesdorfmann.mosby3.mvp.MvpView
import lol.domain.models.SkinOfChampion

interface ChampionSkinView : MvpView {

    fun showSkins(skins: MutableList<SkinOfChampion>)
    fun showStartDownloadSkin(skinName: String)
}