package lol.presentation.champion.skin

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import lol.domain.interactor.champion.ChampionDetailUseCase
import lol.domain.interactor.champion.DownloadSkinUseCase
import lol.domain.models.SkinOfChampion
import javax.inject.Inject

class ChampionSkinPresenter@Inject constructor(private val championDetailUseCase: ChampionDetailUseCase,
                                               private val downloadSkinUseCase: DownloadSkinUseCase)  : MvpBasePresenter<ChampionSkinView>() {

    fun loadSkins(championId: String)
    {
        ifViewAttached { view ->
            championDetailUseCase.getChampionSkins(championId)
                    .subscribe({ skins ->
                        view.showSkins(skins)
                    })
        }
    }

    fun downloadSkin(skin: SkinOfChampion)
    {
        ifViewAttached { view ->
            downloadSkinUseCase.execute(skin.skinUrl, skin.skinFileName)
                    .subscribe({
                        view.showStartDownloadSkin(skin.skinName)
                    })
        }
    }
}