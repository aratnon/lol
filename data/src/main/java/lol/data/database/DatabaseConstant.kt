package lol.data.database

class DatabaseConstant {
    companion object {
        const val VERSION = 1
        const val DATABASE_NAME = "cache.db"
        const val TABLE_CHAMPION = "champions"
        const val TABLE_ITEM ="item"
        const val TABLE_RUNE ="rune"

        const val SELECT_FROM = "SELECT * FROM "
        const val SELECT_CHAMPION_BY_ID = SELECT_FROM + TABLE_CHAMPION
    }
}