package lol.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import lol.data.database.models.RuneDataEntity


@Dao
interface RuneDao {

    @Query("SELECT * FROM rune")
    fun getAllRuneData(): Flowable<List<RuneDataEntity>>

    @Query("SELECT * FROM rune WHERE id = :runeId")
    fun getRuneData(runeId: Int): Flowable<RuneDataEntity>

    @Query("SELECT * FROM rune WHERE category = :category AND slotIndex = :slotIndex")
    fun getAllRuneDataByCategoryAndSlotIndex(category: String, slotIndex: Int): List<RuneDataEntity>

//    @Query("SELECT * FROM item WHERE id IN(:runeIds)")
//    fun getMultipleRuneData(runeIds: List<String>): Flowable<List<RuneDataEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRune(runeData: RuneDataEntity)
}