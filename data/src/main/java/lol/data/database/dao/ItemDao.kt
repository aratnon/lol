package lol.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import lol.data.database.models.ItemDataEntity


@Dao
interface ItemDao {

    @Query("SELECT * FROM item")
    fun getAllItemData(): Flowable<List<ItemDataEntity>>

    @Query("SELECT * FROM item WHERE id = :itemId")
    fun getAllItemData(itemId:String): Flowable<ItemDataEntity>

    @Query("SELECT * FROM item WHERE id IN(:itemIds)")
    fun getMultipleItemData(itemIds: List<String>): Flowable<List<ItemDataEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(itemData: ItemDataEntity)
}