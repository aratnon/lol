package lol.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single
import lol.data.database.DatabaseConstant
import lol.data.database.models.ChampionDataEntity

@Dao
interface ChampionDao {

    @Query("SELECT * FROM champions WHERE id = :championId")
    fun getChampionData(championId: String): Flowable<ChampionDataEntity>

    @Query("SELECT * FROM champions")
    fun getAllChampionData(): Flowable<List<ChampionDataEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChampionData(championData: ChampionDataEntity)
}