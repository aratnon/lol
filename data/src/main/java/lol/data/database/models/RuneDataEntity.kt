package lol.data.database.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import lol.data.database.DatabaseConstant

@Entity(tableName = DatabaseConstant.TABLE_RUNE)
data class RuneDataEntity(
        @PrimaryKey val id: String,
        val category: String,
        val slotIndex: Int,
        val data: String
)