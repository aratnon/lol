package lol.data.database.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import lol.data.database.DatabaseConstant

@Entity(tableName = DatabaseConstant.TABLE_ITEM)
data class ItemDataEntity(
        @PrimaryKey val id: String,
        val data: String
)