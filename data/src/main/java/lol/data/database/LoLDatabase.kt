package lol.data.database
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import lol.data.database.dao.ChampionDao
import lol.data.database.dao.ItemDao
import lol.data.database.dao.RuneDao
import lol.data.database.models.ChampionDataEntity
import lol.data.database.models.ItemDataEntity
import lol.data.database.models.RuneDataEntity

@Database(entities = [ChampionDataEntity::class, ItemDataEntity::class, RuneDataEntity::class], version = DatabaseConstant.VERSION, exportSchema = true)
abstract class LoLDatabase: RoomDatabase(){

    abstract fun championDao(): ChampionDao
    abstract fun itemDao(): ItemDao
    abstract fun runeDao(): RuneDao

    companion object {
        fun buildDatabase(context: Context): LoLDatabase {
            return Room.databaseBuilder(context, LoLDatabase::class.java, DatabaseConstant.DATABASE_NAME).build()
        }
    }
}