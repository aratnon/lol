package lol.data.database

import android.content.Context
import es.dmoral.prefs.Prefs
import io.reactivex.Completable
import io.reactivex.Single
import lol.domain.repository.PrefsHelper

class PrefsHelperImpl (context: Context): PrefsHelper {

    private val LOL_VERSION_KEY: String = "LOL_VERSION"
    private val HAS_INIT_DATA_KEY: String = "HAS_INIT_DATA"

    private val GDPR_CONSENT_AGREEMENT_KEY = "GDPR_CONSENT_AGREEMENT"

    private val prefs: Prefs = Prefs.with(context)

    override fun getLoLVersion(): Single<String>
        = Single.just(prefs.read(LOL_VERSION_KEY))

    override fun setLoLVersion(version: String) {
        prefs.write(LOL_VERSION_KEY, version)
    }

    override fun hasInitData(): Boolean
            = prefs.contains(HAS_INIT_DATA_KEY) && prefs.readBoolean(HAS_INIT_DATA_KEY)

    override fun setHasInitData(hasInitData: Boolean){
        prefs.writeBoolean(HAS_INIT_DATA_KEY, hasInitData)
    }

    override fun hasConsent(): Boolean
        = prefs.contains(GDPR_CONSENT_AGREEMENT_KEY)

    override fun hasAgreeConsent(): Boolean
        = prefs.readBoolean(GDPR_CONSENT_AGREEMENT_KEY)

    override fun setConsent(agree: Boolean): Completable = Completable.fromAction({
        prefs.writeBoolean(GDPR_CONSENT_AGREEMENT_KEY, agree)
    })
}