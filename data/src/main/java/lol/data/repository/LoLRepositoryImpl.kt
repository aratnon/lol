package lol.data.repository

import android.content.Context
import android.os.Environment
import com.google.gson.Gson
import lol.data.database.LoLDatabase
import lol.data.database.models.ChampionDataEntity
import lol.data.database.models.ItemDataEntity
import lol.domain.models.ChampionData
import lol.domain.models.ItemData
import lol.domain.repository.LoLRepository
import org.json.JSONObject
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.charset.Charset
import io.reactivex.*
import android.app.DownloadManager
import android.net.Uri
import lol.data.database.models.RuneDataEntity
import lol.domain.models.RuneData
import lol.domain.repository.LoLRemoteService
import lol.domain.repository.PrefsHelper
import lol.domain.repository.RuneImageRepository
import lol.domain.rx.RxSchedulers
import org.json.JSONArray


class LoLRepositoryImpl(private val context: Context,
                        private val gson: Gson,
                        private val rxSchedulers: RxSchedulers,
                        private val prefsHelper: PrefsHelper,
                        private val lolDatabase: LoLDatabase,
                        private val loLRemoteService: LoLRemoteService,
                        private val runeImageRepository: RuneImageRepository) : LoLRepository
{

    override fun initData(): Completable {
        if(!prefsHelper.hasInitData())
        {
            return loLRemoteService.downloadLoLVersion()
                    .flatMap { initLoLVersion() }
                    .flatMapCompletable { version ->
                        loLRemoteService.downloadLolData(version)
                                .flatMapCompletable { lolData ->
                                    clearDatabase()
                                            .andThen(initChampionData(lolData.championString))
                                            .andThen(initItemData(lolData.itemString))
                                            .andThen(initRuneData(lolData.runeString))
                                            .andThen(setFinishInitData())
                                            .andThen(runeImageRepository.init())
                                }
                    }

//            return loLRemoteService.downloadLolData()
//                    .flatMapCompletable { lolData -> clearDatabase()
//                            .andThen(initChampionData(lolData.championString))
//                            .andThen(initItemData(lolData.itemString))
//                            .andThen(initRuneData(lolData.runeString))
//                            .andThen(initLoLVersion())
//                            .andThen(runeImageRepository.init())
//                    }
        }
        return runeImageRepository.init()
    }

//    override fun initData(): Completable {
//        if(!prefsHelper.hasInitData()) {
//            val loadChampion = loLRemoteService.downloadChampionData()
//                    .flatMapCompletable { data ->
//                        initChampionData(data)
//                    }
//
//            val loadItem = loLRemoteService.downloadItemData()
//                    .flatMapCompletable { data ->
//                        initItemData(data)
//                    }
//
//            val loadRune = loLRemoteService.downloadRuneData()
//                    .flatMapCompletable { data ->
//                        initRuneData(data)
//                    }
//
//            return loadChampion
//                    .andThen(loadItem)
//                    .andThen(loadRune)
//                    .andThen(initLoLVersion())
//                    .andThen(runeImageRepository.init())
//        }
//        else
//        {
//            return runeImageRepository.init()
//        }
//
////        return Completable.fromAction({
////            if(!prefsHelper.hasInitData())
////            {
//////                initChampionData()
//////                initItemData()
//////                initRuneData()
////
////            }
////            else
////                Timber.d("has data")
////        }).andThen(runeImageRepository.init())
//    }

    private fun clearDatabase(): Completable
    {
        return Completable.fromAction({ lolDatabase.clearAllTables() })
                .observeOn(rxSchedulers.io())
    }

    private fun initLoLVersion(): Single<String>
    {
        return loLRemoteService.downloadLoLVersion()
                .map { version ->
                    Timber.d("lol version: $version")
                    prefsHelper.setLoLVersion(version)
                    version
                }.observeOn(rxSchedulers.io())
    }

    private fun setFinishInitData(): Completable
    {
        return Completable.fromAction({
            prefsHelper.setHasInitData(true)
        }).observeOn(rxSchedulers.io())
    }

    private fun initChampionData(json: String): Completable
    {
        return Completable.fromAction({
            val jsonObject = JSONObject(json)
            val dataObject = jsonObject.getJSONObject("data")
            for (key in dataObject.keys())
            {
                val data = dataObject[key]
                val championDataEntity = ChampionDataEntity(key, data.toString())
                lolDatabase.championDao().insertChampionData(championDataEntity)
            }
        }).subscribeOn(rxSchedulers.io())
    }

    private fun initChampionData()
    {
        val json = context.assets.open("champion.json").bufferedReader().use{
            it.readText()
        }

        val jsonObject = JSONObject(json)

        val version = jsonObject.getString("version")
        prefsHelper.setLoLVersion(version)
        prefsHelper.setHasInitData(true)

        val dataObject = jsonObject.getJSONObject("data")
        for (key in dataObject.keys())
        {
            val data = dataObject[key]
            val championDataEntity = ChampionDataEntity(key, data.toString())
            lolDatabase.championDao().insertChampionData(championDataEntity)
        }
    }

    private fun initItemData(json: String): Completable
    {
        return Completable.fromAction({
            val jsonObject = JSONObject(json)
            val dataObject = jsonObject.getJSONObject("data")
            for (key in dataObject.keys()) {
                val data = dataObject[key]
                val itemDataEntity = ItemDataEntity(key, data.toString())
                lolDatabase.itemDao().insertItem(itemDataEntity)
            }
        }).subscribeOn(rxSchedulers.io())
    }

    private fun initItemData()
    {
        val json = context.assets.open("item.json").bufferedReader().use{
            it.readText()
        }

        val jsonObject = JSONObject(json)
        val dataObject = jsonObject.getJSONObject("data")
        for (key in dataObject.keys()) {
            val data = dataObject[key]
            val itemDataEntity = ItemDataEntity(key, data.toString())
            lolDatabase.itemDao().insertItem(itemDataEntity)
        }
    }

    private fun initRuneData(data: String): Completable
    {
        return Completable.fromAction({
            val array = JSONArray(data)
            for(index in 0 until array.length())
            {
                val runePageObject = array.getJSONObject(index)
                val slotArray = runePageObject.getJSONArray("slots")
                val category = runePageObject.getString("key")
                for(slotIndex in 0 until slotArray.length())
                {
                    val slot = slotArray.getJSONObject(slotIndex)
                    val runes = slot.getJSONArray("runes")
                    for(runeIndex in 0 until runes.length())
                    {
                        val rune = runes.getJSONObject(runeIndex)
                        val id = rune.getString("id")
                        val data = rune.toString()
                        val runeDataEntity = RuneDataEntity(id, category, slotIndex, data)
                        lolDatabase.runeDao().insertRune(runeDataEntity)
                    }
                }
            }
        }).subscribeOn(rxSchedulers.io())
    }

    private fun initRuneData()
    {
        val json = context.assets.open("rune_reforge.json").bufferedReader().use{
            it.readText()
        }

        val array = JSONArray(json)
        for(index in 0 until array.length())
        {
            val runePageObject = array.getJSONObject(index)
            val slotArray = runePageObject.getJSONArray("slots")
            val category = runePageObject.getString("key")
            for(slotIndex in 0 until slotArray.length())
            {
//                Timber.d("category: $category slotIndex: $slotIndex")
                val slot = slotArray.getJSONObject(slotIndex)
                val runes = slot.getJSONArray("runes")
                for(runeIndex in 0 until runes.length())
                {
                    val rune = runes.getJSONObject(runeIndex)
                    val id = rune.getString("id")
                    val data = rune.toString()
                    val runeDataEntity = RuneDataEntity(id, category, slotIndex, data)
                    lolDatabase.runeDao().insertRune(runeDataEntity)
//                    Timber.d("rune: $data")
                }
            }
        }
    }

    override fun getChampionData(championId: String): Flowable<ChampionData> {
        return lolDatabase.championDao()
                .getChampionData(championId)
                .map { entity -> gson.fromJson(entity.data, ChampionData::class.java) }
    }

    override fun getAllChampionData(): Flowable<MutableList<ChampionData>> {
        return lolDatabase.championDao()
                .getAllChampionData()
                .map{ entities ->
                    val championData: MutableList<ChampionData> = mutableListOf()
                    for(entity in entities) championData.add(gson.fromJson(entity.data, ChampionData::class.java))
                    championData.sortBy { data -> data.name }

                    championData
                }

    }

    override fun getAllItemData(): Flowable<MutableList<ItemData>> {
        return lolDatabase.itemDao()
                .getAllItemData()
                .map { entities ->
                    val itemData: MutableList<ItemData> = mutableListOf()
                    for(entity in entities)
                        itemData.add(gson.fromJson(entity.data, ItemData::class.java))
                    itemData.sortBy { data -> data.id }

                    itemData
                }
    }

    override fun getItemData(itemId: String): Flowable<ItemData> {
        return lolDatabase.itemDao()
                .getAllItemData(itemId)
                .map { entity ->
                    gson.fromJson(entity.data, ItemData::class.java)
                }
    }

    override fun getMultipleItemData(itemIds: List<String>): Flowable<MutableList<ItemData>> {
        return lolDatabase.itemDao()
                .getMultipleItemData(itemIds)
                .map { entities ->
                    val itemData: MutableList<ItemData> = mutableListOf()
                    for(entity in entities)
                        itemData.add(gson.fromJson(entity.data, ItemData::class.java))
                    itemData.sortBy { data -> data.id }

                    itemData
                }
    }

    override fun getRune(runeId: Int): Flowable<RuneData> {
        return lolDatabase.runeDao().getRuneData(runeId)
                .map { runeDataEntity ->
                    val rune = gson.fromJson(runeDataEntity.data, RuneData::class.java)
                    rune.runeImageUrl = getRuneImageUrl(rune.icon)
                    rune
                }
    }

    override fun getAllRuneDataByCategory(category: String): Flowable<MutableList<MutableList<RuneData>>> {
        return Flowable.fromCallable {
            val runesByCategory: MutableList<MutableList<RuneData>> = mutableListOf()
            for(slotIndex in 0 until 4)
            {
                val runeEntities = lolDatabase.runeDao()
                        .getAllRuneDataByCategoryAndSlotIndex(category, slotIndex)

                Timber.d("runeEntities: ${runeEntities.size}")

                val runeData: MutableList<RuneData> = mutableListOf()
                for(entity in runeEntities)
                {
                    val rune = gson.fromJson(entity.data, RuneData::class.java)
                    rune.runeImageUrl = getRuneImageUrl(rune.icon)
                    runeData.add(rune)
                }

                runeData.sortBy { data -> data.id }
                runesByCategory.add(runeData)
            }

            runesByCategory
        }
    }

    private fun getRuneImageUrl(runeImagePath: String): String
        = "https://ddragon.leagueoflegends.com/cdn/img/$runeImagePath"


    override fun getRuneCategoryImageId(category: String): Flowable<Int> {
        return when(category)
        {
            "Sorcery" -> Flowable.just(runeImageRepository.getRuneImage(8200))
            "Inspiration" -> Flowable.just(runeImageRepository.getRuneImage(8300))
            "Precision" -> Flowable.just(runeImageRepository.getRuneImage(8000))
            "Domination" -> Flowable.just(runeImageRepository.getRuneImage(8100))
            "Resolve" -> Flowable.just(runeImageRepository.getRuneImage(8400))
            else -> Flowable.empty()
        }
    }

    override fun downloadFile(url: String, fileName: String): Completable {
        return Completable.fromAction({
            val request = DownloadManager.Request(Uri.parse(url))

            request.setTitle(fileName)
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)

            val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            manager.enqueue(request)
        })
    }

    @Throws(IOException::class)
    private fun getResource(id: Int, context: Context): ByteArray {
        val resources = context.resources
        val inStream = resources.openRawResource(id)

        val bout = ByteArrayOutputStream()

        val readBuffer = ByteArray(4 * 1024)

        inStream.use { inputStream ->
            var read: Int
            do {
                read = inputStream.read(readBuffer, 0, readBuffer.size)
                if (read == -1) {
                    break
                }
                bout.write(readBuffer, 0, read)
            } while (true)

            return bout.toByteArray()
        }
    }

    // reads a string resource
    @Throws(IOException::class)
    fun getStringResource(id: Int, encoding: Charset): String {
        return String(getResource(id, context), encoding)
    }

    // reads an UTF-8 string resource
    @Throws(IOException::class)
    fun getStringResource(id: Int): String {
        return String(getResource(id, context), Charset.forName("UTF-8"))
    }

    private fun testZipFile()
    {
//        val file = context.assets.open("data.zip").
//        val zipFile = ZipFile(file)
    }
}
