package lol.data.repository

import android.content.Context
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.FirebaseStorage
import lol.domain.repository.LoLRemoteService
import io.reactivex.Flowable
import durdinapps.rxfirebase2.RxFirebaseStorage
import io.reactivex.Single
import lol.domain.models.LolDataModel
import lol.domain.rx.RxSchedulers
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.InputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipFile


class LolRemoteServiceImpl constructor(private val context: Context,
                                       private val firebaseStorage: FirebaseStorage,
                                       private val rxSchedulers: RxSchedulers): LoLRemoteService {
    private val cacheSize = (1024 * 1024).toLong()

    override fun downloadLoLVersion(): Single<String> {
        return RxFirebaseStorage.getBytes(firebaseStorage.getReference("version.txt"), cacheSize)
                .map { data -> String(data) }
                .toSingle()
                .observeOn(rxSchedulers.io())
    }

    override fun downloadLolData(version: String): Single<LolDataModel> {
        val file = File(context.cacheDir.absolutePath + File.pathSeparator + "data_$version.zip")
        return RxFirebaseStorage.getFile(firebaseStorage.getReference("data_$version.zip"), file)
                .map { getLolDataModelFromZipFile(version, file) }
                .observeOn(rxSchedulers.io())
    }

    private fun getLolDataModelFromZipFile(version: String, file: File): LolDataModel
    {
        val zipFile = ZipFile(file)
        val lolDataModel = LolDataModel()

        lolDataModel.championString = getStringFromZipEntry(zipFile, "data_$version/champion.json")
        lolDataModel.itemString = getStringFromZipEntry(zipFile, "data_$version/item.json")
        lolDataModel.runeString = getStringFromZipEntry(zipFile, "data_$version/rune_reforge.json")
        return lolDataModel
    }

    private fun getStringFromZipEntry(zipFile: ZipFile, entryName: String): String
    {
        val entry = zipFile.getEntry(entryName)
        val stream = zipFile.getInputStream(entry)
        return getStringFromInputStream(stream)
    }

    private fun getStringFromInputStream(inputStream: InputStream): String {
        val bout = ByteArrayOutputStream()

        val readBuffer = ByteArray(4 * 1024)

        inputStream.use {
            var read: Int
            do {
                read = it.read(readBuffer, 0, readBuffer.size)
                if (read == -1) {
                    break
                }
                bout.write(readBuffer, 0, read)
            } while (true)

            return String(bout.toByteArray())
        }
    }
}