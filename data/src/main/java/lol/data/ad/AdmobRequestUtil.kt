package lol.data.ad

import com.google.android.gms.ads.AdRequest
import android.os.Bundle
import com.google.ads.mediation.admob.AdMobAdapter
import timber.log.Timber


class AdmobRequestUtil {
    companion object {
        fun createAdRequest(personalized: Boolean): AdRequest
        {
            val npa = if (personalized) "0" else "1"
            Timber.d(npa)

            val extras = Bundle()
            extras.putString("npa", npa)

            return AdRequest.Builder()
                    .addTestDevice("3BFEE35FC1F1C60CC9D8F132D8429282")
                    .addNetworkExtrasBundle(AdMobAdapter::class.java, extras)
                    .build()
        }
    }
}