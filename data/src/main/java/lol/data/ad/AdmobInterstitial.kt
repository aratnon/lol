package lol.data.ad

import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.InterstitialAd
import io.reactivex.Single
import lol.domain.ad.BaseInterstitial

class AdmobInterstitial constructor(private val interstitialAd: InterstitialAd): BaseInterstitial {

    override fun show(listener: BaseInterstitial.InterstitialAdListener) {
        interstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                super.onAdClosed()
                listener.onAdClosed()
            }
        }
        interstitialAd.show()
    }

    override fun load(personalized: Boolean) {
        interstitialAd.loadAd(AdmobRequestUtil.createAdRequest(personalized))
    }

    override fun isLoaded(): Boolean = interstitialAd.isLoaded
}